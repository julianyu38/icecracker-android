package com.YRH.IceCracker;

import org.cocos2d.nodes.CCNode;

public class G {
	public static float _scaleX = 1f;
	public static float _scaleY = 1f;

	public static float _realScaleX = 1f;
	public static float _realScaleY = 1f;

	public static float PadVSPhoneX = 1024f / 480;
	public static float PadVSPhoneY = 768f / 320;

	public static float WIN_W;
	public static float WIN_H;
	

	public static float _getX(float x) {
		return _scaleX * x;
	}

	public static float _getY(float y) {
		return _scaleY * y;
	}

	public static float _getPosX(float x) {
		return _realScaleX * x;
	}

	public static float _getPosY(float y) {
		return _realScaleY * y;
	}

	public static String _getImg(String imgName) {
		return String.format("%s%s%s", _imgDirPath, imgName, ".png");
	}

	public static String _getFont(String fntName) {
		return String.format("%s%s%s", _fntDirPath, fntName, ".ttf");
	}

	public static void setScale(CCNode node) {
		node.setScaleX(_scaleX);
		node.setScaleY(_scaleY);
	}

	public static void setScale(CCNode node, float scaleFactor) {
		node.setScaleX(_scaleX * scaleFactor);
		node.setScaleY(_scaleY * scaleFactor);
	}

	public static void setScale(CCNode node, boolean bSmall) {
		float scale = bSmall ? (_scaleX < _scaleY ? _scaleX : _scaleY)
			: (_scaleX > _scaleY ? _scaleX : _scaleY);
		node.setScale(scale);
	}

	public static final float 	TIME_TICKS= 1f/30;
	public static final int 	MAX_DIGIT = 5;
	public static final float 	DEFAULT_W = 1024;
	public static final float 	DEFAULT_H = 768;

	private static final String _imgDirPath = "image/";
	private static final String _fntDirPath = "font/";
	
	public static final int 	PRESS_OPACITY	= 255;
	public static final int 	RELEASE_OPACITY	= 200;
	public static final int 	SHADOW_OPACITY	= 80;
	public static final int 	OPTION_OPACITY  = SHADOW_OPACITY;
	public static final int 	MAX_KNIFE_INSCREEN = 20;	
	public static final int 	MSG_OTHER  	= 0;
	public static final int 	MSG_INFO  	= 1;
}