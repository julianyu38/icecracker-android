package com.YRH.IceCracker;

import org.cocos2d.nodes.CCDirector;
import org.cocos2d.opengl.CCGLSurfaceView;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Toast;

import com.YRH.Others.GameSound;
import com.YRH.Scenes.LogoView;

public class GameActivity extends Activity {
	private CCGLSurfaceView mGLSurfaceView;
	
    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mGLSurfaceView = new CCGLSurfaceView(this);
		CCDirector.sharedDirector().setScreenSize(G.DEFAULT_W, G.DEFAULT_H);
		CCDirector.sharedDirector().setDeviceOrientation(CCDirector.kCCDeviceOrientationPortrait);
		InitParam();
	    setContentView(mGLSurfaceView, createLayoutParams());
	}
    
    @Override public void onStart() {
        super.onStart();        
        CCDirector.sharedDirector().attachInView(mGLSurfaceView);
        CCDirector.sharedDirector().setDisplayFPS(false);
        CCDirector.sharedDirector().setAnimationInterval(1.0f / 60);
        CCDirector.sharedDirector().runWithScene(LogoView.scene());
    }

    @Override public void onPause() {
        super.onPause();
        CCDirector.sharedDirector().pause();
		finish();
    }

    @Override public void onResume() {
        super.onResume();
        CCDirector.sharedDirector().resume();
    }

    @Override public void onDestroy() {
        super.onDestroy();
        GameSound.stopEffect();
        GameSound.stopBG(); 
        CCDirector.sharedDirector().end();
    }

	private void InitParam() {
		Display display = getWindowManager().getDefaultDisplay();
		G.WIN_W = display.getWidth();
		G.WIN_H = display.getHeight();
		
		G._scaleX = G.WIN_W / G.DEFAULT_W;
		G._scaleY = G.WIN_H / G.DEFAULT_H;

		G._realScaleX = G.WIN_W / 480;
		G._realScaleY = G.WIN_H / 320;
	}

	private LayoutParams createLayoutParams() {
        final DisplayMetrics pDisplayMetrics = new DisplayMetrics();
		CCDirector.sharedDirector().getActivity().getWindowManager().getDefaultDisplay().getMetrics(pDisplayMetrics);
		
		
		final float mRatio = (float)G.DEFAULT_W / G.DEFAULT_H;
		final float realRatio = (float)pDisplayMetrics.widthPixels / pDisplayMetrics.heightPixels;

		final int width, height;
		if(realRatio < mRatio) {
			width = pDisplayMetrics.widthPixels;
			height = Math.round(width / mRatio);
		} else {
			height = pDisplayMetrics.heightPixels;
			width = Math.round(height * mRatio);
		}

		final LayoutParams layoutParams = new LayoutParams(width, height);

		layoutParams.gravity = Gravity.CENTER;
		return layoutParams;
	}
	
	public Handler mH = new Handler() {
		@Override public void handleMessage(Message msg) {
			switch(msg.what) {
			case G.MSG_INFO: case G.MSG_OTHER:
				try {
					Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
					intent.putExtra(SearchManager.QUERY, "http://www.dreamapps.com.au/");
					startActivity(intent);
				} catch(Exception e) {
					Toast.makeText(GameActivity.this,
						R.string.MSG_OPENFAIL, Toast.LENGTH_SHORT).show();
				}
				break;
			}
		}
	};
}