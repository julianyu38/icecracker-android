package com.YRH.Scenes;

import org.cocos2d.actions.ease.CCEaseBounceOut;
import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCMoveBy;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.config.ccMacros;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.menus.CCMenu;
import org.cocos2d.menus.CCMenuItemImage;
import org.cocos2d.menus.CCMenuItemToggle;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.CGPoint;
import org.cocos2d.utils.javolution.MathLib;

import android.view.MotionEvent;

import com.YRH.IceCracker.G;
import com.YRH.IceCracker.GameActivity;
import com.YRH.Objects.KnifeSprite;
import com.YRH.Others.GameData;
import com.YRH.Others.GameSound;

public class MainMenuScene extends CCLayer {
    CCSprite _background;
    CCSprite _playSprite;
    CCSprite _optionSprite;
    CCSprite _infoSprite;
    CCSprite _otherSprite;

	boolean _isPressed;
	int _menuSelected;
	
	int _menuState;
	
	CGPoint _prevPoint = CGPoint.zero();
	float _menuTime;
	float _pastTime;
	int _selectKnife;
	boolean _isEmptyKnife;

	int _swardHissID;

	public static CCScene scene() {
		CCScene scene = CCScene.node();
		scene.addChild(new MainMenuScene());
		return scene;
	}
	
	public MainMenuScene() {
		super();
		setIsTouchEnabled(true);
		_menuState = enum_state_menu;
		_isPressed = false;
		
		GameSound.stopBG();
		GameSound.playMainBG();

		_background = CCSprite.sprite("main_background@2x.png");
		G.setScale(_background);
		_playSprite = CCSprite.sprite("main_menu_play@2x.png");
		G.setScale(_playSprite);
		_optionSprite = CCSprite.sprite("main_option_background@2x.png");
		G.setScale(_optionSprite);
		_infoSprite = CCSprite.sprite("main_menu_info@2x.png");
		G.setScale(_infoSprite, false);
		_otherSprite = CCSprite.sprite("main_menu_other@2x.png");
		G.setScale(_otherSprite);

		for (int i = 0; i < G.MAX_KNIFE_INSCREEN; i++) {
			KnifeSprite knifeSprite = new KnifeSprite();
			addChild(knifeSprite, enum_tag_knifes + i, enum_tag_knifes + i);
		}
        
		_background.setPosition(G.WIN_W / 2, G.WIN_H / 2);
		_playSprite.setPosition(
			G.WIN_W / 2 + G._getPosX(MENU_PLAY_X),
			G.WIN_H / 2 + G._getPosY(MENU_PLAY_Y));
		_optionSprite.setPosition(
			G.WIN_W + G._getPosX(MENU_OPTION_X),
			G._getPosY(MENU_OPTION_Y));
		_infoSprite.setPosition(
			G._getPosX(MENU_INFO_X),
			G._getPosY(MENU_INFO_Y));
		_otherSprite.setPosition(
			G.WIN_W / 2 + G._getPosX(MENU_OTHER_X),
			G.WIN_H / 2 + G._getPosY(MENU_OTHER_Y));
		
		_playSprite.setOpacity(G.RELEASE_OPACITY);
		_infoSprite.setOpacity(G.RELEASE_OPACITY);
		_otherSprite.setOpacity(G.RELEASE_OPACITY);

		addChild(_background, 		enum_tag_background, 	enum_tag_background);
		addChild(_playSprite, 		enum_tag_play, 			enum_tag_play);
		addChild(_optionSprite, 	enum_tag_option, 		enum_tag_option);
		addChild(_infoSprite, 		enum_tag_info, 			enum_tag_info);
		addChild(_otherSprite, 		enum_tag_other, 		enum_tag_other);
		
		float option_w = _optionSprite.getContentSize().width;
		float option_h = _optionSprite.getContentSize().height;
		
		CCMenuItemToggle itemSound = CCMenuItemToggle.item(
			this, "onClickEffect",
			CCMenuItemImage.item("main_option_sound_unselect@2x.png", "main_option_sound_unselect@2x.png"),
			CCMenuItemImage.item("main_option_sound_select@2x.png", "main_option_sound_select@2x.png"));
		itemSound.setPosition(
           	option_w / 2 + OPTION_SOUND_X * G.PadVSPhoneX,
           	option_h / 2 + OPTION_SOUND_Y * G.PadVSPhoneY);

		CCMenuItemToggle itemMusic = CCMenuItemToggle.item(
			this, "onClickSound",
			CCMenuItemImage.item("main_option_music_unselect@2x.png", "main_option_music_unselect@2x.png"),
			CCMenuItemImage.item("main_option_music_select@2x.png", "main_option_music_select@2x.png"));
		itemMusic.setPosition(
			option_w / 2 + OPTION_MUSIC_X * G.PadVSPhoneX,
        	option_h / 2 + OPTION_MUSIC_Y * G.PadVSPhoneY);

		CCMenuItemToggle itemVib = CCMenuItemToggle.item(
			this, "onClickVib",
			CCMenuItemImage.item("main_option_vibration_unselect@2x.png", "main_option_vibration_unselect@2x.png"),
			CCMenuItemImage.item("main_option_vibration_select@2x.png", "main_option_vibration_select@2x.png"));
		itemVib.setPosition(
    		option_w / 2 + OPTION_VIBRATION_X * G.PadVSPhoneX,
    		option_h / 2 + OPTION_VIBRATION_Y * G.PadVSPhoneY);

		CCMenu menu = CCMenu.menu(itemSound, itemMusic, itemVib);
		menu.setPosition(0, 0);
		_optionSprite.addChild(menu);
		
		_isEmptyKnife = false;
		_menuTime = 0; _isPressed = false;
		
		if(!GameSound.enableVibrator()) {
			ccMacros.CCLOG("YRH", "Not Support Vibrator");
		}
		schedule("updateNow", 0.1f);
	}
	
	public void onClickEffect(Object sender) {
		CCMenuItemToggle selected = (CCMenuItemToggle) sender;
		boolean flag;
		if (selected.selectedIndex()==1)
			flag = true;
		else flag = false;
		GameData.setEffect(flag);
		GameSound.effectBtnClick();
		GameSound.vib(GameSound.VIB_BUTTON);
	}

	public void onClickSound(Object sender) {
		GameSound.effectBtnClick();
		CCMenuItemToggle selected = (CCMenuItemToggle) sender;
		boolean flag;
		if (selected.selectedIndex()==1)
			flag = true;
		else flag = false;
		GameData.setSound(flag);
		GameSound.playMainBG();
		GameSound.vib(GameSound.VIB_BUTTON);
	}

	public void onClickVib(Object sender) {
		GameSound.effectBtnClick();
		CCMenuItemToggle selected = (CCMenuItemToggle) sender;
		boolean flag;
		if (selected.selectedIndex()==1)
			flag = true;
		else	flag = false;
		GameData.setVibration(flag);
		GameSound.vib(GameSound.VIB_BUTTON);
	}

	
	public void updateNow(float deltaTime) {
		_menuTime += deltaTime;
		if (_isPressed) _pastTime += deltaTime;
		else _pastTime = 0;
	}
	
	public void appearPanel() {
		_optionSprite.runAction(
			CCEaseBounceOut.action(CCMoveBy.action(1,
			CGPoint.ccp(0, G._getPosY(OPTION_MOVE)))));
	}
	
	public void changeState() {
		_menuState = enum_state_option;
	}
	
	public void disappearPanel() {
		_optionSprite.runAction(
			CCEaseBounceOut.action(CCMoveBy.action(1,
			CGPoint.ccp(0, -G._getPosY(OPTION_MOVE)))));
	}
	
	public void restoreState() {
		_menuState = enum_state_menu;
	}
	
	public void pushOptionPanel() {
		_menuState = enum_state_temp;
		_background.setOpacity(G.PRESS_OPACITY);
		_playSprite.setOpacity(G.RELEASE_OPACITY);
		_infoSprite.setOpacity(G.RELEASE_OPACITY);
		_otherSprite.setOpacity(G.RELEASE_OPACITY);
		runAction(CCSequence.actions(
			 CCCallFunc.action(this, "disappearPanel"),
			 CCDelayTime.action(0.8f),
			 CCCallFunc.action(this, "restoreState")));	
	}
	
	public void popOptionPanel() {
		_menuState = enum_state_temp;
		_background.setOpacity(G.OPTION_OPACITY);
		_playSprite.setOpacity(G.OPTION_OPACITY);
		_infoSprite.setOpacity(G.OPTION_OPACITY);
		_otherSprite.setOpacity(G.OPTION_OPACITY);
		runAction(CCSequence.actions(
			 CCCallFunc.action(this, "appearPanel"),
			 CCDelayTime.action(0.8f),
			 CCCallFunc.action(this, "changeState")));
	}
	
	public int getMenu(float x, float y) {
		float dx, dy;
		dx = x - (G.WIN_W / 2 + G._getPosX(MENU_PLAY_X));
		dy = y - (G.WIN_H / 2 + G._getPosY(MENU_PLAY_Y));
		if (MathLib.sqrt(dx * dx + dy * dy) < G._getPosX(MENU_PLAY_R))
            return enum_press_play;
		
		dx = x - G._getPosX(MENU_INFO_X);
		dy = y - G._getPosY(MENU_INFO_Y);
		if (MathLib.sqrt(dx * dx + dy * dy) < G._getPosX(MENU_INFO_R))
            return enum_press_info;
		
		dx = x - (G.WIN_W / 2 + G._getPosX(MENU_OTHER_X));
		dy = y - (G.WIN_H / 2 + G._getPosY(MENU_OTHER_Y));
		if (MathLib.sqrt(dx * dx + dy * dy) < G._getPosX(MENU_OTHER_R))
            return enum_press_other;
		
		if ((G.WIN_W - G._getPosX(OPTION_WIDTH)) < x &&
			y < (G._getPosY(OPTION_HEIGHT) / 2 + G._getPosY(MENU_OPTION_Y)))
            return enum_press_option;
		
		return enum_press_none;
	}
	
	public int getOption(float x, float y) {
		float dx, dy;
		float refX = _optionSprite.getPosition().x;
		float refY = _optionSprite.getPosition().y;
		if (x < G.WIN_W - G._getPosX(OPTION_WIDTH) ||
			y > G._getPosY(OPTION_HEIGHT))
            return enum_option_cancel;

		dx = x - (refX + G._getPosX(OPTION_LABEL_X));
		dy = y - (refY + G._getPosY(OPTION_LABEL_Y));
		if (G._getPosX((-OPTION_LABEL_WIDTH) / 2 - 2) < dx 	&&
			dx < G._getPosX(OPTION_LABEL_WIDTH / 2 + 2)		&&
			G._getPosY((-OPTION_LABEL_HEIGHT) / 2 - 2) < dy &&
			dy < G._getPosY(OPTION_LABEL_HEIGHT / 2 + 2))
            return enum_option_cancel;
		
		dx = x - (refX + OPTION_SOUND_X);
		dy = y - (refY + OPTION_SOUND_Y);
		if (((-OPTION_SOUND_WIDTH) / 2 - 2) < dx 	&&
			dx < (OPTION_SOUND_WIDTH / 2 + 2)		&&
			((-OPTION_SOUND_HEIGHT) / 2 - 2) < dy 	&&
			dy < (OPTION_SOUND_HEIGHT / 2 + 2)) {
            return enum_option_sound;
        }
		
		dx = x - (refX + G._getPosX(OPTION_MUSIC_X));
		dy = y - (refY + G._getPosY(OPTION_MUSIC_Y));
		if (G._getPosX((-OPTION_MUSIC_WIDTH) / 2 - 2) < dx 	&&
			dx < G._getPosX(OPTION_MUSIC_WIDTH / 2 + 2)		&&
			G._getPosY((-OPTION_MUSIC_HEIGHT) / 2 - 2) < dy &&
			dy < G._getPosY(OPTION_MUSIC_HEIGHT / 2 + 2)) {
            return enum_option_music;
        }
		dx = x - (refX + G._getPosX(OPTION_VIBRATION_X));
		dy = y - (refY + G._getPosY(OPTION_VIBRATION_Y));
		if (G._getPosX((-OPTION_VIBRATION_WIDTH) / 2 - 2) < dx 	&&
			dx < G._getPosX(OPTION_VIBRATION_WIDTH / 2 + 2)		&&
			G._getPosX((-OPTION_VIBRATION_HEIGHT) / 2 - 2) < dy &&
			dy < G._getPosX(OPTION_VIBRATION_HEIGHT / 2 + 2)) {
            return enum_option_vibration;
		}
		
		return enum_option_none;
	}
	
	@Override public boolean ccTouchesBegan(MotionEvent event) {
		CGPoint location = CCDirector.sharedDirector().convertToGL(
			CGPoint.ccp(event.getX(), event.getY()));
		_prevPoint.set(location);
		switch(_menuState) {
		case enum_state_menu:
			int menuType = getMenu(location.x, location.y);
			_selectKnife = G.MAX_KNIFE_INSCREEN;
			_isPressed = true;
	        _pastTime = LIMIT_PAST_TIME;
			_menuSelected = menuType;
			
			if (_menuSelected != enum_press_option) return false;
			
			switch (_menuSelected) {
			case enum_press_play:
				_playSprite.setOpacity(G.PRESS_OPACITY);
				break;
			case enum_press_option:
				GameSound.effectPopOption();
				popOptionPanel();
				break;
			case enum_press_info:
				_infoSprite.setOpacity(G.PRESS_OPACITY);
				break;
			case enum_press_other:
				_otherSprite.setOpacity(G.PRESS_OPACITY);
				break;
			default:	
                break;
			}
			break;
		case enum_state_option:
			int optionType  = getOption(location.x, location.y);
			if (optionType == enum_option_none) return false;
			switch (optionType) {
			case enum_option_cancel:	
				_menuState = enum_state_temp;
				GameSound.effectPushOption();
				pushOptionPanel();
				return false;
			default: break;
			}
			break;
		}
		return false;
	}
	
	@Override public boolean ccTouchesMoved(MotionEvent event) {
		CGPoint location = CCDirector.sharedDirector().convertToGL(
			CGPoint.ccp(event.getX(), event.getY()));
	
		if (_menuState == enum_state_option) return false;
	
		KnifeSprite tmpKnife = null;
		for (int i = 0; i < G.MAX_KNIFE_INSCREEN; i++) {
			tmpKnife = (KnifeSprite)getChildByTag(enum_tag_knifes + i);
			if (!tmpKnife.isActive) {
				_selectKnife = i;
				break;
			}
		}
		boolean isDraw;
		if ((isDraw = tmpKnife.appearAction(
			_prevPoint.x, _prevPoint.y, location.x,
			location.y, KnifeSprite.enum_knife_0, true))) {
	        _prevPoint.set(location);
	    }
	
		if (_pastTime > LIMIT_PAST_TIME && isDraw) {
			_pastTime = 0;
			_swardHissID = GameSound.effectSwardHiss();
		}
		_isEmptyKnife = true;
		
		int menuType = getMenu(location.x, location.y);
		if (_menuSelected == enum_press_none) {
			_menuSelected = menuType;
		} else if (menuType != enum_press_none && menuType != _menuSelected)
			_menuSelected = enum_press_double;
		return true;
	}
	
	@Override public boolean ccTouchesEnded(MotionEvent event) {
		CGPoint location = CCDirector.sharedDirector().convertToGL(
			CGPoint.ccp(event.getX(), event.getY()));
		
		if (_menuState == enum_state_option) return false;
		KnifeSprite tmpKnife = null;
		for (int i = 0; i < G.MAX_KNIFE_INSCREEN; i++) {
			tmpKnife = (KnifeSprite) getChildByTag(enum_tag_knifes + i);
			if (!tmpKnife.isActive) {
				_selectKnife = i;
				break;
			}
		}
		tmpKnife.appearAction(_prevPoint.x, _prevPoint.y,
			location.x, location.y, KnifeSprite.enum_knife_0, true);
		if (_isEmptyKnife) {
	        GameSound.stopSwardHiss(_swardHissID);
			GameSound.effectKnife();
			_isEmptyKnife = false;
			_pastTime = 0;
		}
		
		if (!_isPressed) return false;
		_isPressed = false;
	
		int menuType = getMenu(location.x, location.y);
		if (_menuSelected == enum_press_none) _menuSelected = menuType;
		else if (menuType != enum_press_none && menuType != _menuSelected)
			_menuSelected = enum_press_double;
		switch (_menuSelected) {
			case enum_press_play:	
				_playSprite.setVisible(true);
				_playSprite.setOpacity(G.PRESS_OPACITY);
	            GameSound.effectBtnMenu();
				CCDirector.sharedDirector().replaceScene(
					CCFadeTransition.transition(0.6f, PlayScene.scene()));
				break;
			case enum_press_option:	
	            _optionSprite.setOpacity(G.RELEASE_OPACITY);	
	            break;
			case enum_press_info:	
				_infoSprite.setOpacity(G.RELEASE_OPACITY);
	            GameSound.effectBtnMenu(); sendMsg(G.MSG_INFO);
				break;
			case enum_press_other:	
				_otherSprite.setOpacity(G.RELEASE_OPACITY);
	            GameSound.effectBtnMenu(); sendMsg(G.MSG_OTHER);
				break;
			default:			
	            break;
		}
		_menuSelected = enum_press_none;
		return false;
	}
	
	private void sendMsg(final int msg) {
		GameActivity act = (GameActivity) CCDirector.theApp;
		act.mH.sendEmptyMessage(msg);
	}

	private static final int enum_tag_background 	= 0;
	private static final int enum_tag_play 			= 1;
	private static final int enum_tag_option 		= 4;
	private static final int enum_tag_info 			= 5;
	private static final int enum_tag_other 		= 6;
	private static final int enum_tag_knifes 		= 7;

	private static final int enum_state_menu 		= 0;
	private static final int enum_state_option 		= 1;
	private static final int enum_state_temp 		= 2;

	private static final int enum_press_none 		= 0;
	private static final int enum_press_play 		= 1;
	private static final int enum_press_option 		= 2;
	private static final int enum_press_info 		= 3;
	private static final int enum_press_other 		= 4;
	private static final int enum_press_double 		= 5;

	private static final int enum_option_none 		= 0;
	private static final int enum_option_cancel 	= 1;
	private static final int enum_option_sound 		= 2;
	private static final int enum_option_music 		= 3;
	private static final int enum_option_vibration 	= 4;
	
	
	private static final int MENU_PLAY_R           	= 50;
	private static final int MENU_PLAY_X           	= -2;
	private static final int MENU_PLAY_Y           	= -64;
	
	private static final int MENU_OTHER_R           = 30;
	private static final int MENU_OTHER_X           = -2;
	private static final int MENU_OTHER_Y           = -130;
	
	private static final int MENU_OPTION_X			= -60;
	private static final int MENU_OPTION_Y			= -28;
	private static final int MENU_OPTION_WIDTH		= 112;
	private static final int OPTION_HEIGHT          = 139;
	private static final int OPTION_WIDTH			= MENU_OPTION_WIDTH;
	
	private static final int OPTION_MOVE            = 95;
	
	private static final int OPTION_LABEL_X         = 8;
	private static final int OPTION_LABEL_Y         = 65;
	private static final int OPTION_LABEL_WIDTH     = 100;
	private static final int OPTION_LABEL_HEIGHT    = 32;
	
	private static final int OPTION_SOUND_X         = 1;
	private static final int OPTION_SOUND_Y         = -18;
	private static final int OPTION_SOUND_WIDTH     = 69;
	private static final int OPTION_SOUND_HEIGHT    = 19;
	
	private static final int OPTION_MUSIC_X         = -2;
	private static final int OPTION_MUSIC_Y         = 10;
	private static final int OPTION_MUSIC_WIDTH     = 62;
	private static final int OPTION_MUSIC_HEIGHT    = 20;
	
	private static final int OPTION_VIBRATION_X		= 2;
	private static final int OPTION_VIBRATION_Y		= -48;
	private static final int OPTION_VIBRATION_WIDTH	= 99;
	private static final int OPTION_VIBRATION_HEIGHT= 20;
	
	private static final int MENU_INFO_R            = 25;
	private static final int MENU_INFO_X            = 30;
	private static final int MENU_INFO_Y            = 29;
	
	private static final int LIMIT_PAST_TIME		= 1;
}