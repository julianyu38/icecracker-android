package com.YRH.Scenes;

import java.util.Arrays;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.CGPoint;
import org.cocos2d.utils.javolution.MathLib;

import android.view.MotionEvent;

import com.YRH.IceCracker.G;
import com.YRH.Objects.ComboSprite;
import com.YRH.Objects.IceSprite;
import com.YRH.Objects.KnifeSprite;
import com.YRH.Objects.PauseSprite;
import com.YRH.Objects.PlayHighScoreSprite;
import com.YRH.Objects.PlayOptionSprite;
import com.YRH.Objects.PlayScoreSprite;
import com.YRH.Objects.PlayTimerSprite;
import com.YRH.Objects.ScoreSprite;
import com.YRH.Others.GameData;
import com.YRH.Others.GameSound;

public class PlayScene extends CCLayer {
	CCSprite backSprite;
    CCSprite pauseBtnSprite;
    CCSprite pengiunLife1, pengiunLife2, pengiunLife3;
    PlayTimerSprite playTimerSprite;
    PlayScoreSprite playScoreSprite;
    PlayHighScoreSprite playHighScoreSprite;
	PauseSprite pauseSprite;
	PlayOptionSprite playOptionSprite;
	ScoreSprite scoreSprite;

	int _gameState;
	boolean _isPressed;
	int _pressedState;
	int _pressedStateShadow;
	
	boolean _isClicked;
	
	boolean[] _isValidKnifeColor = new boolean[MAX_KNIFE_COLOR];
	CGPoint[] prevPoint = new CGPoint[MAX_KNIFE_COLOR];
	CGPoint[] prevDrawPoint = new CGPoint[MAX_KNIFE_COLOR];
	float[] _pastTime = new float[MAX_KNIFE_COLOR];
	boolean[] _isEmptySlash = new boolean[MAX_KNIFE_COLOR];
	int[] _iceComboCount = new int[MAX_KNIFE_COLOR];
	
	int _score;
    int _decreasePenguinLifeCount;
	
	float _intervalThresholdTime;
	float _intervalTime, _gameTime, _restTime;
	float _iceComboX, _iceComboY;
    
    int _penguinAppearNum;
    float _penguinAppearTime;
    boolean GLOBAL_gamePlaying;
    boolean GLOBAL_displayPauseMenu;


	public static CCScene scene() {
		CCScene scene = CCScene.node();
		scene.addChild(new PlayScene());
		return scene;
	}
	
	@Override public void onExit() {
		_isEmptySlash = _isValidKnifeColor = null;
		prevPoint = prevDrawPoint = null;
		_pastTime = null; _iceComboCount = null;
		super.onExit();
		System.gc();
	}

	public PlayScene() {
		super();
		setIsTouchEnabled(true);
		backSprite = CCSprite.sprite("play_background@2x.png"); G.setScale(backSprite);
		pauseBtnSprite = CCSprite.sprite("play_btn_pause_normal@2x.png");
		G.setScale(pauseBtnSprite, false);
        pengiunLife1 = CCSprite.sprite("penguin_life@2x.png"); G.setScale(pengiunLife1);
        pengiunLife2 = CCSprite.sprite("penguin_life@2x.png"); G.setScale(pengiunLife2);
        pengiunLife3 = CCSprite.sprite("penguin_life@2x.png"); G.setScale(pengiunLife3);
        
		playTimerSprite = new PlayTimerSprite();
		playScoreSprite = new PlayScoreSprite();
		playHighScoreSprite = new PlayHighScoreSprite();
		pauseSprite = new PauseSprite();
		playOptionSprite = new PlayOptionSprite();
		scoreSprite = new ScoreSprite();
		
		for (int i = 0; i < G.MAX_KNIFE_INSCREEN; i++) {
			KnifeSprite knifeSprite = new KnifeSprite();
			addChild(knifeSprite, enum_tag_knifes + i, enum_tag_knifes + i);
		}
		
		for (int i = 0; i < MAX_ICE_INGAME; i++) {
			IceSprite ice = new IceSprite();
			addChild(ice, enum_tag_ices + i, enum_tag_ices + i);
		}
		
		for (int i = 0; i < MAX_BONUS_INSCREEN; i++) {
			ComboSprite comboSprite = new ComboSprite();
			addChild(comboSprite, enum_tag_combo + i, enum_tag_combo + i);
		}
		
		backSprite.setPosition(G.WIN_W / 2, G.WIN_H / 2);
		pauseBtnSprite.setPosition(
			G._getPosX(PAUSE_BTN_X),
			G._getPosY(PAUSE_BTN_Y));
        pengiunLife1.setPosition(
        	G._getPosX(PENGUIN_LIFE_1_X),
        	G._getPosY(PENGUIN_LIFE_1_Y));
        pengiunLife2.setPosition(
        	G._getPosX(PENGUIN_LIFE_2_X),
        	G._getPosY(PENGUIN_LIFE_2_Y));
        pengiunLife3.setPosition(
        	G._getPosX(PENGUIN_LIFE_3_X),
        	G._getPosY(PENGUIN_LIFE_3_Y));
		playTimerSprite.setPosition(
			G.WIN_W + G._getPosX(PLAY_TIMER_X),
			G.WIN_H + G._getPosY(PLAY_TIMER_Y));
		playScoreSprite.setPosition(
			G._getPosX(PLAY_SCORE_X),
			G.WIN_H + G._getPosY(PLAY_SCORE_Y));
		playHighScoreSprite.setPosition(
			G._getPosX(PLAY_HIGHSCORE_X),
			G.WIN_H + G._getPosY(PLAY_HIGHSCORE_Y));
		pauseSprite.setPosition(
			G.WIN_W / 2 + G._getPosX(PANEL_X),
			G.WIN_H / 2 + G._getPosY(PANEL_Y));
		playOptionSprite.setPosition(
			G.WIN_W / 2 + G._getPosX(PANEL_X),
			G.WIN_H / 2 + G._getPosY(PANEL_Y));
		scoreSprite.setPosition(
			G.WIN_W / 2 + G._getPosX(PANEL_X),
			G.WIN_H / 2 + G._getPosY(PANEL_Y));
		
		addChild(backSprite,		0);
		addChild(playScoreSprite, 	1);
		addChild(playHighScoreSprite,2);
		addChild(playTimerSprite, 	3);
		addChild(pauseBtnSprite, 	4);
        addChild(pengiunLife1, 		5);
        addChild(pengiunLife2, 		6);
        addChild(pengiunLife3, 		7);
		addChild(pauseSprite, 		8);
		addChild(scoreSprite, 		9);
		addChild(playOptionSprite, 	10);

		pauseBtnSprite.setOpacity(G.RELEASE_OPACITY);
		Arrays.fill(prevPoint, CGPoint.zero());
		Arrays.fill(prevDrawPoint, CGPoint.zero());
		reset();
	}
	
	public void reset() {
		initGame();
		resumeGame();
	}
	
	public void initGame() {
		_gameTime = 0;
		_intervalTime = 0;
		_intervalThresholdTime = INTERVAL_MAX;
		_isPressed = false;
		_score = 0;
	    _penguinAppearNum = 1;
	    _penguinAppearTime = 10;

	    Arrays.fill(_isValidKnifeColor, false);
	    Arrays.fill(_isEmptySlash, true);
	    Arrays.fill(_iceComboCount, 0);
	
	    _decreasePenguinLifeCount = 0;
	    pengiunLife1.setVisible(true);
	    pengiunLife2.setVisible(true);
	    pengiunLife3.setVisible(true);
	
	    playTimerSprite.setVisible(true);
	    playTimerSprite.initTimer();
	    playTimerSprite.setTime(GAME_TIME);
	    GameSound.stopBG();
	    GameSound.playGameBG();
	
		playHighScoreSprite.setScore(GameData.getBestScore());
		playScoreSprite.clearScore();
	}
	
	public void updateNow(float deltaTime) {
	    if (GLOBAL_displayPauseMenu) {
	        if (_gameState != enum_game_state_tmp) {
				_gameState = enum_game_state_tmp;
	            runAction(CCSequence.actions(
					CCCallFunc.action(this, "pauseGame"),
					CCCallFunc.action(this, "appearPauseSprite"),
					CCDelayTime.action(APPEAR_TIME),
					CCCallFunc.action(this, "changeStatePause")));
	        }
	        GLOBAL_displayPauseMenu = false;
	    }
		_gameTime += deltaTime;
	
		for (int i = 0; i < MAX_KNIFE_COLOR; i++) {
			if (!_isValidKnifeColor[i]) continue;
			_pastTime[i] += deltaTime;
			if (_pastTime[i] > LIMIT_PAST_TIME * 1.2f && !_isEmptySlash[i]) {
				_pastTime[i] = LIMIT_PAST_TIME;
				_isEmptySlash[i] = true;
				if (_iceComboCount[i] > 2) {
					ComboSprite tmpComboSprite = null;
					for (int j = 0; j < MAX_BONUS_INSCREEN; j++) {
						tmpComboSprite = (ComboSprite)getChildByTag(enum_tag_combo + j);
						if (!tmpComboSprite.isActive) break;
					}
					tmpComboSprite.appearEffect(
						(_iceComboCount[i] - 2) * 10,
						_iceComboX, _iceComboY);
					addScore((_iceComboCount[i] - 2) * 10);
				}
				_iceComboCount[i] = 0;
			}
		}
	    _intervalTime += deltaTime;
	    _restTime = MathLib.max(GAME_TIME - _gameTime, 0);
	    playTimerSprite.setTime(_restTime);
	    if (_intervalTime >= _intervalThresholdTime) {
	        _intervalTime -= _intervalThresholdTime;
	        _intervalThresholdTime = MathLib.max((INTERVAL_MAX - INTERVAL_MIN) *
	        	(_penguinAppearTime - _gameTime) / _penguinAppearTime, 0);
	        _intervalThresholdTime += INTERVAL_MIN;
	        throwIces();
	    }
	    
	    if (_restTime == 0) {
	        GLOBAL_gamePlaying = false;
	        if (GameData.getBestScore() < _score) {
	            GameData.setBestScore(_score);
	        }
	        _gameState = enum_game_state_tmp;
	        scoreSprite.setScore(_score);
	        GameSound.effectGameEnd();
	        runAction(CCSequence.actions(
                 CCCallFunc.action(this, "pausing"),
                 CCDelayTime.action(1),
                 CCCallFunc.action(this, "stopGame"),
                 CCCallFunc.action(this, "appearScoreSprite"),
                 CCDelayTime.action(APPEAR_TIME),
                 CCCallFunc.action(this, "changeStateScore")));
	    } else {
	        GLOBAL_gamePlaying = true;
	    }
	}
	
	public void slashIces(float x1, float y1, float x2, float y2, int knifeColor) {
		if (x1 == x2 && y1 == y2)  return;
		int combo = 0;
		float refX, refY, minR = 100000;
		boolean isFail = false;
		refX = prevPoint[knifeColor].x; refY = prevPoint[knifeColor].y;
		for (int i = 0; i < MAX_ICE_INGAME; i++) {
			IceSprite tmpIce = (IceSprite)getChildByTag(enum_tag_ices + i);
			int splitType = 0;
			if ((splitType = tmpIce.splitAction(x1, y1, x2, y2)) != 0) {
				if (MathLib.sqrt((tmpIce.getPosition().x - refX) * (tmpIce.getPosition().x - refX) +
					(tmpIce.getPosition().y - refY) * (tmpIce.getPosition().y - refY)) < minR) {
					_iceComboX = tmpIce.getPosition().x;
					_iceComboY = tmpIce.getPosition().y;
				}
				if (tmpIce.iceKind == IceSprite.MAX_ICE) {
	                GameSound.effectPenguin();
					isFail = true;
				} else {
	                combo++;
					if ((splitType & IceSprite.enum_first_split) ==
						IceSprite.enum_first_split) addScore(4);
					if ((splitType & IceSprite.enum_second_first_split) ==
						IceSprite.enum_second_first_split) addScore(6);
					if ((splitType & IceSprite.enum_second_second_split) ==
						IceSprite.enum_second_second_split) addScore(6);
					if (tmpIce.isFullSplit()) {
						addScore( 10);
						ComboSprite tmpComboSprite = null;
						for (int j = 0; j < MAX_BONUS_INSCREEN; j++) {
							tmpComboSprite = (ComboSprite)getChildByTag(enum_tag_combo + j);
							if (!tmpComboSprite.isActive) break;
						}
						tmpComboSprite.appearEffect(10, 
							tmpIce.getPosition().x,
							tmpIce.getPosition().y);
					}
				}
			}
		}
		if (isFail) {
			_iceComboCount[knifeColor] = 0;
			_gameState = enum_game_state_play;
			Arrays.fill(_isValidKnifeColor, false);
			runAction(CCSequence.actions(
				 CCCallFunc.action(this, "pausing"),
				 CCDelayTime.action(2),
				 CCCallFunc.action(this, "resuming"),
				 CCCallFunc.action(this, "changeStatePlay")));
			return;
		}
		if (combo != 0) {
			_iceComboCount[knifeColor] += combo;
			KnifeSprite knife = null;
			for (int i = 0; i < G.MAX_KNIFE_INSCREEN; i++) {
				KnifeSprite tmp = (KnifeSprite)getChildByTag(enum_tag_knifes + i);
				if (!tmp.isActive) {
					knife = tmp; break;
				}
			}
			if(knife!=null)
				knife.splitAction(x1, y1, x2, y2, knifeColor);
		}
	}
	    
	public void addScore(int addedScore) {
	    if (_restTime != 0) {
	        _score += addedScore;
	        playScoreSprite.addScore(addedScore);
	        playHighScoreSprite.updateScore(_score);	
	    }
	}
	
	public void decreasePenguinLife() {
	    _decreasePenguinLifeCount++;
	    GameSound.vib(GameSound.VIB_BOMB);
	    if (_decreasePenguinLifeCount == 1) {
	        pengiunLife1.setVisible(false);
	    }
	    if (_decreasePenguinLifeCount == 2) {
	        pengiunLife2.setVisible(false);
	    }
	    if (_decreasePenguinLifeCount == 3) {
	        pengiunLife3.setVisible(false);
	        GLOBAL_gamePlaying = false;
	        if (GameData.getBestScore() < _score) {
	            GameData.setBestScore(_score);
	        }
	        _gameState = enum_game_state_tmp;
	        scoreSprite.setScore(_score);
	        GameSound.effectGameEnd();
	        runAction(CCSequence.actions(
                 CCCallFunc.action(this, "pausing"),
                 CCDelayTime.action(1),
                 CCCallFunc.action(this, "stopGame"),
                 CCCallFunc.action(this, "appearScoreSprite"),
                 CCDelayTime.action(APPEAR_TIME),
                 CCCallFunc.action(this, "changeStateScore")));
	    }
	}
	
	public void throwIces() {
		float maxInScreen =  MathLib.max(1, (_gameTime % MAX_ICE_INSCREEN+1));
		int throwCount = 0;
		while (throwCount < maxInScreen) {
			for (int i = 0; i < MAX_ICE_INGAME; i++) {
				IceSprite ices = (IceSprite)getChildByTag(enum_tag_ices + i);
				if (!ices.isActive) {
					if (_intervalThresholdTime < (INTERVAL_MIN + INTERVAL_MAX) / 2)
	                    ices.startFlyingWithPenguin(DELAY_VARIENCE, _penguinAppearNum);
					else  ices.startFlying(DELAY_VARIENCE);
					throwCount++;
					if (throwCount >= maxInScreen) return;
				}
			}
		}
	}
	
	public void playTouchesBegan(float x, float y) {
		float dx, dy;
		boolean valid = true;
		for (int i = 0; i < MAX_KNIFE_COLOR; i++) {
			if (_isValidKnifeColor[i]) {
				valid = false; break;
			}
		}
		if (valid) {
			dx = x - G._getPosX(PAUSE_BTN_X);
			dy = y - G._getPosY(PAUSE_BTN_Y);
			if (MathLib.sqrt(dx * dx + dy * dy) < G._getPosX(PAUSE_BTN_R)) {
				_isPressed = true;
				GameSound.effectBtnClick();
				_pressedState = enum_play_press_pause_btn;
				_pressedStateShadow = _pressedState;
                CCSprite tmp = CCSprite.sprite("play_btn_pause_press@2x.png");
                pauseBtnSprite.setTexture(tmp.getTexture());
                pauseBtnSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
				pauseBtnSprite.setOpacity(G.PRESS_OPACITY);
			}
		} System.gc();
		if (_isPressed) return;
		
		for (int j = 0;  j < MAX_KNIFE_COLOR; j++) {
			if (!_isValidKnifeColor[j]) {
				_isValidKnifeColor[j] = true;
				_pastTime[j] = LIMIT_PAST_TIME;
				prevPoint[j].set(x, y);
				prevDrawPoint[j].set(x, y);
				_iceComboCount[j] = 0;
				return;
			}
		}
	}
					 
	public void pauseTouchesBegan(float x, float y) {
		pauseSprite.touchesBegan(x, y);
	}
	
	public void optionTouchesBegan(float x, float y) {
		playOptionSprite.touchesBegan(x, y);
	}
	
	public void scoreTouchesBegan(float x, float y) {
		scoreSprite.touchesBegan(x, y);
	}
	
	@Override public boolean ccTouchesBegan(MotionEvent event) {
		CGPoint loc = CCDirector.sharedDirector().convertToGL(
			CGPoint.ccp(event.getX(), event.getY()));
		switch (_gameState) {
		case enum_game_state_play:		
            playTouchesBegan(loc.x, loc.y);	
            break;
		case enum_game_state_pause:	
            pauseTouchesBegan(loc.x, loc.y);	
            break;
		case enum_game_state_option:
            optionTouchesBegan(loc.x, loc.y);
            break;
		case enum_game_state_score:
            scoreTouchesBegan(loc.x, loc.y);
            break;
		default:	
            break;
		}
		return true;
	}
	
	public void playTouchesMoved(float x, float y) {
		float dx, dy;
		if (_isPressed) {
			dx = x - G._getPosX(PAUSE_BTN_X);
			dy = y - G._getPosY(PAUSE_BTN_Y);
			if (MathLib.sqrt(dx * dx + dy * dy) < G._getPosX(PAUSE_BTN_R)) {
				if (_pressedStateShadow != enum_play_press_pause_btn) {
					_pressedStateShadow = enum_play_press_pause_btn;
					pauseBtnSprite.setOpacity(G.PRESS_OPACITY);
				}
			}
			else {
				if (_pressedStateShadow == enum_play_press_pause_btn) {
					_pressedStateShadow = enum_play_press_none;
					pauseBtnSprite.setOpacity(G.RELEASE_OPACITY);
				}
			}
			return;
		}
		boolean[] isHandled = new boolean[MAX_KNIFE_COLOR];
		Arrays.fill(isHandled, false);
		
		boolean handle = false;
		for (int j = 0;  j < MAX_KNIFE_COLOR; j++) {
			if (_isValidKnifeColor[j]
			    /*&& CGPoint.equalToPoint(_prevPoint[j], prevLocation)*/) {
				handle = isHandled[j] = true;
				CGPoint tmpPoint = CGPoint.ccp(prevPoint[j].x ,prevPoint[j].y);
				prevPoint[j].set(x, y);
				boolean validKnife = false;
				KnifeSprite tmpKnife = null;
				for (int i = 0; i < G.MAX_KNIFE_INSCREEN; i++) {
					tmpKnife = (KnifeSprite) getChildByTag(enum_tag_knifes + i);
					if (!tmpKnife.isActive) {
						validKnife = true; break;
					}
				}
				_isEmptySlash[j] = false;
				if (validKnife) {
					if (tmpKnife.appearAction(
						prevDrawPoint[j].x, prevDrawPoint[j].y,
						x, y, j, false)) {
						prevDrawPoint[j].set(x, y);
						if (_pastTime[j] > LIMIT_PAST_TIME) {
                            GameSound.effectKnife();
							_pastTime[j] = 0;
						}
						_isEmptySlash[j] = true;
					}
				}
				slashIces(tmpPoint.x, tmpPoint.y, x, y, j);
				break;
			}
		}
		if (!handle) {
			for (int k = 0;  k < MAX_KNIFE_COLOR; k++) {
				if (!_isValidKnifeColor[k]) {
					_isValidKnifeColor[k] = true;
					_pastTime[k] = 0;
					prevPoint[k].set(x, y);
					prevDrawPoint[k].set(x, y);
					_iceComboCount[k] = 0;
					isHandled[k] = true;
					break;
				}
			}
		}
	}
	
	public void pauseTouchesMoved(float x, float y) {
		pauseSprite.touchesMoved(x, y);
	}
	
	public void optionTouchesMoved(float x, float y) {
		playOptionSprite.touchesMoved(x, y);
	}
	
	public void scoreTouchesMoved(float x, float y) {
		scoreSprite.touchesMoved(x, y);
	}
	
	@Override public boolean ccTouchesMoved(MotionEvent event) {
		CGPoint loc = CCDirector.sharedDirector().convertToGL(
			CGPoint.ccp(event.getX(), event.getY()));
		
		switch (_gameState) {
			case enum_game_state_play:
	            playTouchesMoved(loc.x, loc.y);	
	            break;
			case enum_game_state_pause:
	            pauseTouchesMoved(loc.x, loc.y);
	            break;
			case enum_game_state_option:
	            optionTouchesMoved(loc.x, loc.y);
	            break;
			case enum_game_state_score:
	            scoreTouchesMoved(loc.x, loc.y);
	            break;
			default:	
	            break;
		}
		return true;
	}
	
	public void playTouchesEnded(float x, float y) {
		float dx, dy, dr;
		
		if (_isPressed) {
			dx = x - G._getPosX(PAUSE_BTN_X);
			dy = y - G._getPosY(PAUSE_BTN_Y);
			dr = G._getPosX(PAUSE_BTN_R);
			if (MathLib.sqrt(dx * dx + dy * dy) < dr) {
	            CCSprite tmp = CCSprite.sprite("play_btn_pause_normal@2x.png");
	            pauseBtnSprite.setTexture(tmp.getTexture());
	            pauseBtnSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
	            
				pauseBtnSprite.setOpacity(G.RELEASE_OPACITY);
				_gameState = enum_game_state_tmp;
				runAction(CCSequence.actions(
					 CCCallFunc.action(this, "pauseGame"),
					 CCCallFunc.action(this, "appearPauseSprite"),
					 CCDelayTime.action(APPEAR_TIME),
					 CCCallFunc.action(this, "changeStatePause")));
			}
			else {
	            CCSprite tmp = CCSprite.sprite("play_btn_pause_normal@2x.png");
	            pauseBtnSprite.setTexture(tmp.getTexture());
	            pauseBtnSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
				pauseBtnSprite.setOpacity(G.RELEASE_OPACITY);
			}
			_isPressed = false;
			_pressedState = enum_play_press_none;
			return;
		}
		
		for (int j = 0;  j < MAX_KNIFE_COLOR; j++) {
			if (_isValidKnifeColor[j]
                   /*&& CGPoint.equalToPoint(_prevPoint[j], prevLocation)*/) {
				boolean validKnife = false;
				KnifeSprite tmpKnife = null;
				for (int i = 0; i < G.MAX_KNIFE_INSCREEN; i++) {
					tmpKnife = (KnifeSprite) getChildByTag(enum_tag_knifes + i);
					if (!tmpKnife.isActive) {
						validKnife = true; break;
					}
				}
				
				if (validKnife) {
					boolean isKnifeSound = false;
					if (_iceComboCount[j] == 0)
                        isKnifeSound = true;
					tmpKnife.appearAction(
						prevDrawPoint[j].x, prevDrawPoint[j].y,
						x, y, j, isKnifeSound);				
				}
			}
			_isValidKnifeColor[j] = false;
		}
		
		for (int i = 0; i < MAX_KNIFE_COLOR; i++) {
			_isEmptySlash[i] = true;
			if (_iceComboCount[i] > 2) {
				ComboSprite tmpComboSprite = null;
				for (int j = 0; j < MAX_BONUS_INSCREEN; j++) {
					tmpComboSprite = (ComboSprite)getChildByTag(enum_tag_combo + j);
					if (!tmpComboSprite.isActive) {
	                    break;
	                }
				}
                tmpComboSprite.appearEffect(
                	(_iceComboCount[i] - 2) * 10,
                	_iceComboX + G._getPosX(5),
                	_iceComboY + G._getPosY(5));
				addScore((_iceComboCount[i] - 2) * 10);
			}
			if (_iceComboCount[i] != 0) {
	            GameSound.effectKnifeSlashed();
	        }
			_iceComboCount[i] = 0;
		}
	}
	
	public void pauseTouchesEnded(float x, float y) {
		switch (pauseSprite.touchesEnded(x, y)) {
			case PauseSprite.enum_pause_continue:
				_gameState = enum_game_state_tmp;
				runAction(CCSequence.actions(
					 CCCallFunc.action(this, "disappearPauseSprite"),
					 CCCallFunc.action(this, "resumeGame"),
					 CCDelayTime.action(APPEAR_TIME),
					 CCCallFunc.action(this, "changeStatePlay")));
				break;
			case PauseSprite.enum_pause_option:
				_gameState = enum_game_state_tmp;
				runAction(CCSequence.actions(
					 CCCallFunc.action(this, "disappearPauseSprite"),
					 CCCallFunc.action(this, "appearOptionSprite"),
					 CCDelayTime.action(APPEAR_TIME),
					 CCCallFunc.action(this, "changeStateOption")));
				break;
			case PauseSprite.enum_pause_exit:
	            GLOBAL_gamePlaying = false;
				CCDirector.sharedDirector().replaceScene(
					CCFadeTransition.transition(0.8f, MainMenuScene.scene()));
				break;
			default:	
	            break;
		}
	}
	
	public void optionTouchesEnded(float x, float y) {
		switch (playOptionSprite.touchesEnded(x, y)) {
		case PlayOptionSprite.enum_play_option_exit:
			_gameState = enum_game_state_tmp;
			runAction(CCSequence.actions(
				 CCCallFunc.action(this, "disappearOptionSprite"),
				 CCCallFunc.action(this, "appearPauseSprite"),
				 CCDelayTime.action(APPEAR_TIME),
				 CCCallFunc.action(this, "changeStatePause")));
			break;
		default:
            break;
		}
	}
	
	public void scoreTouchesEnded(float x, float y) {
		switch (scoreSprite.touchesEnded(x, y)) {
		case ScoreSprite.enum_score_reply:
			_gameState = enum_game_state_tmp;
			runAction(CCSequence.actions(
				 CCCallFunc.action(this, "disappearScoreSprite"),
				 CCDelayTime.action(APPEAR_TIME),
				 CCCallFunc.action(this, "reset"),
				 CCCallFunc.action(this, "changeStatePlay")));
			break;
		case ScoreSprite.enum_score_exit:
            GLOBAL_gamePlaying = false;
			CCDirector.sharedDirector().replaceScene(
				CCFadeTransition.transition(0.8f, MainMenuScene.scene()));
			break;
		default:
			break;
		}
	}
	
	@Override public boolean ccTouchesEnded(MotionEvent event) {
		CGPoint loc = CCDirector.sharedDirector().convertToGL(
			CGPoint.ccp(event.getX(), event.getY()));
		
		switch (_gameState) {
		case enum_game_state_play:
            playTouchesEnded(loc.x, loc.y);
            break;
		case enum_game_state_pause:
            pauseTouchesEnded(loc.x, loc.y);
            break;
		case enum_game_state_option:
            optionTouchesEnded(loc.x, loc.y);
            break;
		case enum_game_state_score:
            scoreTouchesEnded(loc.x, loc.y);
            break;
		default: break;
		}
		return true;
	}
	
	public void pausing() {
		unschedule("updateNow");
		
		IceSprite tmpIce;
		for (int i = 0; i < MAX_ICE_INGAME; i++) {
			tmpIce = (IceSprite)getChildByTag(enum_tag_ices + i);
			if (tmpIce.isActive) {
				tmpIce.unscheduleAllSelectors();
			}
		}
	}
	
	public void pauseGame() {
		unschedule("updateNow");
		backSprite.setOpacity(G.SHADOW_OPACITY);
		playScoreSprite.settingOpacity(G.SHADOW_OPACITY);
		playHighScoreSprite.settingOpacity(G.SHADOW_OPACITY);
	    playTimerSprite.settingOpacity(G.SHADOW_OPACITY);
		pauseBtnSprite.setOpacity(G.SHADOW_OPACITY);
		
		IceSprite tmpIce;
		for (int i = 0; i < MAX_ICE_INGAME; i++) {
			tmpIce = (IceSprite)getChildByTag(enum_tag_ices + i);
			if (tmpIce.isActive) {
				tmpIce.pauseSchedulerAndActions();
				tmpIce.settingOpacity(G.SHADOW_OPACITY);
			}
		}
		KnifeSprite tmpKnife;
		for (int i = 0; i < G.MAX_KNIFE_INSCREEN; i++) {
			tmpKnife = (KnifeSprite)getChildByTag(enum_tag_knifes + i);
			if (tmpKnife.isActive) {
				tmpKnife.pauseSchedulerAndActions();
				tmpKnife.setOpacity(tmpKnife.getOpacity() * G.SHADOW_OPACITY / 256);
			}
		}
	}
	
	public void stopGame() {
		unschedule("updateNow");
		backSprite.setOpacity(G.SHADOW_OPACITY);
		playScoreSprite.settingOpacity(G.SHADOW_OPACITY);
		playHighScoreSprite.settingOpacity(G.SHADOW_OPACITY);
	    playTimerSprite.settingOpacity(G.SHADOW_OPACITY);
		pauseBtnSprite.setOpacity(G.SHADOW_OPACITY);
		IceSprite tmpIce;
		for (int i = 0; i < MAX_ICE_INGAME; i++) {
			tmpIce = (IceSprite)getChildByTag(enum_tag_ices + i);
			if (tmpIce.isActive) {
				tmpIce.unscheduleAllSelectors();
				tmpIce.initIce();
			}
		}
		KnifeSprite tmpKnife;
		for (int i = 0; i < G.MAX_KNIFE_INSCREEN; i++) {
			tmpKnife = (KnifeSprite)getChildByTag(enum_tag_knifes + i);
			if (tmpKnife.isActive) {
				tmpKnife.unscheduleAllSelectors();
				tmpKnife.initKnife();
			}
		}
	}
	
	public void resuming() {
		schedule("updateNow");
	}
	
	public void resumeGame() {
		backSprite.setOpacity(G.PRESS_OPACITY);
		playScoreSprite.settingOpacity(G.PRESS_OPACITY);
		playHighScoreSprite.settingOpacity(G.PRESS_OPACITY);
	    playTimerSprite.settingOpacity(G.PRESS_OPACITY);
		pauseBtnSprite.setOpacity(G.RELEASE_OPACITY);
		
		IceSprite tmpIce;
		for (int i = 0; i < MAX_ICE_INGAME; i++) {
			tmpIce = (IceSprite)getChildByTag(enum_tag_ices + i);
			if (tmpIce.isActive) {
				tmpIce.resumeSchedulerAndActions();
				tmpIce.settingOpacity(G.PRESS_OPACITY);
			}
		}
		KnifeSprite tmpKnife;
		for (int i = 0; i < G.MAX_KNIFE_INSCREEN; i++) {
			tmpKnife = (KnifeSprite)getChildByTag(enum_tag_knifes + i);
			if (tmpKnife.isActive) {
	            tmpKnife.resumeSchedulerAndActions();
	        }
		}
		schedule("updateNow");
	}
	
	public void appearPauseSprite() {
		pauseSprite.setVisible(true);
		pauseSprite.appearSprite(APPEAR_TIME);	
	}
	
	public void appearOptionSprite() {
		playOptionSprite.setVisible(true);
		playOptionSprite.appearSprite(APPEAR_TIME);	
	}
	
	public void appearScoreSprite() {
		scoreSprite.setVisible(true);
		scoreSprite.appearSprite(APPEAR_TIME);	
	}
	
	public void disappearPauseSprite() {
		pauseSprite.disappearSprite(APPEAR_TIME);
	}
	
	public void disappearOptionSprite() {
		playOptionSprite.disappearSprite(APPEAR_TIME);
	}
	
	public void disappearScoreSprite() {
		scoreSprite.disappearSprite(APPEAR_TIME);
	}
	
	public void changeStatePause() {
		_gameState = enum_game_state_pause;
	}
	
	public void changeStateOption() {
		_gameState = enum_game_state_option;
	}
	
	public void changeStateScore() {
		_gameState = enum_game_state_score;
	}
	
	public void changeStatePlay() {
		_gameState = enum_game_state_play;
	}
	
	
	
	private static final int MAX_KNIFE_COLOR				= 3;
	
	private static final int enum_tag_ices 					= 120;
	private static final int enum_tag_knifes 				= 200;
	private static final int enum_tag_combo 				= 300;

	private static final int enum_game_state_play 			= 0;
	private static final int enum_game_state_pause			= 1;
	private static final int enum_game_state_option			= 2;
	private static final int enum_game_state_score			= 3;
	private static final int enum_game_state_tmp			= 4;

	private static final int enum_play_press_none 			= 0;
	private static final int enum_play_press_pause_btn		= 1;

	private static final int 	MAX_ICE_INGAME				= 20;
	private static final int 	MAX_ICE_INSCREEN			= 3;
	private static final int 	MAX_BONUS_INSCREEN			= 3;
	
	private static final int 	PANEL_X	  			= 0;
	private static final int 	PANEL_Y    			= 16;
	
	private static final int 	PLAY_TIMER_X		= -42;
	private static final int 	PLAY_TIMER_Y		= -18;
	
	private static final int 	PLAY_SCORE_X		= 41;
	private static final int 	PLAY_SCORE_Y		= -18;
	
	private static final int 	PLAY_HIGHSCORE_X	= 30;
	private static final int 	PLAY_HIGHSCORE_Y	= -39;
	
	private static final int 	PAUSE_BTN_X			= 22;
	private static final int 	PAUSE_BTN_Y			= 18;
	private static final int 	PAUSE_BTN_R			= 35;
	
	private static final int 	PENGUIN_LIFE_1_X   	= 316;
	private static final int 	PENGUIN_LIFE_1_Y   	= 305;
	
	private static final int 	PENGUIN_LIFE_2_X   	= 346;
	private static final int 	PENGUIN_LIFE_2_Y   	= 305;
	
	private static final int 	PENGUIN_LIFE_3_X   	= 376;
	private static final int 	PENGUIN_LIFE_3_Y   	= 305;
	
	private static final int 	GAME_TIME			= 120;
	private static final float 	APPEAR_TIME			= 0.8f;
	
	private static final int 	LIMIT_PAST_TIME		= 1;
	
	private static final int 	INTERVAL_MAX		= 2;
	private static final float 	INTERVAL_MIN		= 1.5f;
	private static final float 	DELAY_VARIENCE		= 0.8f;
}