package com.YRH.Scenes;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.transitions.CCFadeTransition;

import com.YRH.IceCracker.G;

public class LogoView extends CCLayer {
	public static CCScene scene() {
		CCScene scene = CCScene.node();
		scene.addChild(new LogoView());
		return scene;
	}

	public LogoView() {
		super();
		CCSprite bgSP = CCSprite.sprite("Splash@2x.png");
		G.setScale(bgSP);
		bgSP.setPosition(G.WIN_W / 2, G.WIN_H / 2);
		addChild(bgSP);
		runAction(CCSequence.actions(CCDelayTime.action(3),
			CCCallFunc.action(this, "gotoMenuView")));
	}

	public void gotoMenuView() {
		CCDirector.sharedDirector().replaceScene(
			CCFadeTransition.transition(0.8f, MainMenuScene.scene()));
	}
}