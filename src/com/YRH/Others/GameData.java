package com.YRH.Others;

import org.cocos2d.nodes.CCDirector;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class GameData {
	private static int mVib = -1, mSnd = -1, mEff = -1, mBest = -1;
	public static boolean isMultiSplit() {
		SharedPreferences setting = CCDirector.sharedDirector().getActivity()
			.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
		return setting.getBoolean("isMultiSplit", true);
	}
	
	public static void setisMultiSplit(boolean value) {
		Editor editor = CCDirector.sharedDirector().getActivity()
			.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE).edit();
		editor.putBoolean("isMultiSplit",value);
		editor.commit();	
	}
	
	public static boolean getEffect() {
		if(mEff != -1) return (mEff==1 ? true : false); 
		SharedPreferences setting = CCDirector.sharedDirector().getActivity()
			.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
		boolean ret = setting.getBoolean("sound", true);
		mEff = ret ? 1 : 0;
		return ret;
	}
	
	public static void setEffect(boolean value) {
		mEff = value ? 1 : 0;
		Editor editor = CCDirector.sharedDirector().getActivity()
			.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE).edit();
		editor.putBoolean("sound", value);
		editor.commit();	
	}
	
	public static boolean getSound() {
		if(mSnd != -1) return (mSnd==1 ? true : false); 
		SharedPreferences setting = CCDirector.sharedDirector().getActivity()
			.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
		boolean ret = setting.getBoolean("music", true);
		mSnd = ret ? 1 : 0;
		return ret;
	}
	
	public static void setSound(boolean value) {
		mSnd = value ? 1 : 0;
		Editor editor = CCDirector.sharedDirector().getActivity()
			.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE).edit();
		editor.putBoolean("music", value);
		editor.commit();	
	}
	
	public static boolean getVibration() {
		if(mVib != -1) return (mVib==1 ? true : false); 
		SharedPreferences setting = CCDirector.sharedDirector().getActivity()
			.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
		boolean ret = setting.getBoolean("vibration", true);
		mVib = ret ? 1 : 0;
		return ret;
	}
	
	public static void setVibration(boolean value) {
		mVib = value ? 1 : 0;
		Editor editor = CCDirector.sharedDirector().getActivity()
			.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE).edit();
		editor.putBoolean("vibration", value);
		editor.commit();	
	}
	
	public static int getBestScore() {
		if(mBest != -1) return mBest;
		SharedPreferences setting = CCDirector.sharedDirector().getActivity()
			.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
		mBest = setting.getInt("bestScoreGame", 0);
	    return mBest;
	}
	
	public static void setBestScore(int value) {
		mBest = value;
		Editor editor = CCDirector.sharedDirector().getActivity()
			.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE).edit();
        editor.putInt("bestScoreGame", value);
		editor.commit();
	}
	private static final String APP_NAME = "Ice Cracker";
}