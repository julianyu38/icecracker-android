package com.YRH.Others;

import org.cocos2d.nodes.CCDirector;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.utils.javolution.MathLib;

import android.content.Context;
import android.os.Vibrator;

import com.YRH.IceCracker.R;

public class GameSound{
	private static Vibrator mVibrator;

	public static void effectPenguin() {
		if (GameData.getEffect()) {
			SoundEngine.sharedEngine().playEffect(
				CCDirector.sharedDirector().getActivity(), penguin);
	    }
	}
	
	public static void effectBtnMenu() {
		if (GameData.getEffect()) {
			SoundEngine.sharedEngine().playEffect(
				CCDirector.sharedDirector().getActivity(), menu_Btn);
	    }
	}
	
	public static void effectBtnClick() {
		if (GameData.getEffect()) {
			SoundEngine.sharedEngine().playEffect(
				CCDirector.sharedDirector().getActivity(), btn_Click);
	    }
	}
	
	public static void effectPopOption() {
		if (GameData.getEffect()) {
			SoundEngine.sharedEngine().playEffect(
				CCDirector.sharedDirector().getActivity(), pop_Btn);
	    }
	}
	
	public static void effectPushOption() {
		if (GameData.getEffect()) {
			SoundEngine.sharedEngine().playEffect(
				CCDirector.sharedDirector().getActivity(), push_Btn);
	    }
	}
	
	public static void effectThrowIce(int value) {
	    int rID = 1;
		switch (value) {
        case 3: case 5: rID = ice_throw1; break;
        case 4: case 6:	
        case 7:	case 8: rID = ice_throw3; break;
        case 0: case 1: 
        case 2: default: rID = ice_throw2; break;
		}
		if (GameData.getEffect()) {
			SoundEngine.sharedEngine().playEffect(
				CCDirector.sharedDirector().getActivity(), rID);
	    }
	}
	
	public static void effectIceSlashed(int value) {
		int num = 1;
		switch (value) {
		case 0: case 3: num = ice_5; break;
		case 1: case 7: num = ice_1; break;
		case 4: case 6: num = ice_2; break;
		case 5: 		num = ice_3; break;
		case 2: case 8:
		default: 		num = ice_4; break;
		}
		if (GameData.getEffect()) {
			SoundEngine.sharedEngine().playEffect(
				CCDirector.sharedDirector().getActivity(), num);
	    }
	}
	
	public static int effectSwardHiss() {
		if (GameData.getEffect()) {
			SoundEngine.sharedEngine().playEffect(
				CCDirector.sharedDirector().getActivity(), swardhiss);
			return swardhiss;
	    }
		return 0;
	}
	
	public static void stopSwardHiss(int swardHissID) {
		if (GameData.getEffect()) {
			SoundEngine.sharedEngine().stopEffect(
				CCDirector.sharedDirector().getActivity(), swardHissID);
	    }
	}
	
	public static void effectGameEnd() {
		if (GameData.getEffect()) {
	        SoundEngine.sharedEngine().playEffect(
	        	CCDirector.sharedDirector().getActivity(), game_End);
		}
	}
	
	public static void effectKnifeSlashed() {
		if (GameData.getEffect()) {
			SoundEngine.sharedEngine().playEffect(
				CCDirector.sharedDirector().getActivity(), knife_Slashed);
	    }
	}

	public static void effectKnife() {
		int rID = knifeEff[MathLib.random(0, 2)];
		if (GameData.getEffect()) {
			SoundEngine.sharedEngine().playEffect(
				CCDirector.sharedDirector().getActivity(), rID);
	    }
	}
	
	public static void effectBeep() {
		if (GameData.getEffect()) {
			SoundEngine.sharedEngine().playEffect(
				CCDirector.sharedDirector().getActivity(), beep);
	    }
	}
	
	public static void playMainBG() {
		stopBG();
		if (GameData.getSound()) {
			SoundEngine.sharedEngine().playSound(
				CCDirector.sharedDirector().getActivity(), menu_Background, true);
		}
	}
	
	public static void playGameBG() {
		if (GameData.getSound()) {
			SoundEngine.sharedEngine().playSound(
				CCDirector.sharedDirector().getActivity(), game_Background, true);
		}
	}
	
	public static void stopBG() {
		SoundEngine.sharedEngine().realesAllSounds();
	}
	
	public static void stopEffect() {
		SoundEngine.sharedEngine().realesAllEffects();
	}

	// for Vibration!
	public static boolean enableVibrator() {
		mVibrator = (Vibrator) CCDirector.sharedDirector().getActivity()
			.getSystemService(Context.VIBRATOR_SERVICE);
		return mVibrator != null;
	}

	public static void vib(final long pMilliseconds) {
		if (!GameData.getVibration()) return;
		if(mVibrator != null)
			mVibrator.vibrate(pMilliseconds);
	}

	public static void vib(final long[] pPattern, final int pRepeat){
		if (!GameData.getVibration()) return;
		if(mVibrator != null)
			mVibrator.vibrate(pPattern, pRepeat);
	}

	private static final int game_Background = R.raw.game_background;
	private static final int menu_Background = R.raw.menu_background;
	private static final int beep 			 = R.raw.beep;
	private static final int btn_Click		 = R.raw.btn_click;
	private static final int game_End		 = R.raw.game_end;
	private static final int ice_1			 = R.raw.ice_1;
	private static final int ice_2			 = R.raw.ice_2;
	private static final int ice_3			 = R.raw.ice_3;
	private static final int ice_4			 = R.raw.ice_4;
	private static final int ice_5			 = R.raw.ice_5;
	private static final int ice_throw1		 = R.raw.ice_throw1;
	private static final int ice_throw2		 = R.raw.ice_throw2;
	private static final int ice_throw3		 = R.raw.ice_throw3;
	private static final int knife_Slashed	 = R.raw.knife_slashed;
	private static final int menu_Btn		 = R.raw.menu_btn;
	private static final int penguin		 = R.raw.penguin;
	private static final int pop_Btn		 = R.raw.pop_btn;
	private static final int push_Btn		 = R.raw.push_btn;
	private static final int swardhiss		 = R.raw.swardhiss;
	
	private static final int[] knifeEff = {
		R.raw.knife_0, R.raw.knife_1, R.raw.knife_2
	};
	
	public static final long VIB_BUTTON		 = 100;
	public static final long VIB_BOMB		 = 300;
	public static final long VIB_ICE		 = 200;
}