package com.YRH.Objects;

import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGRect;

import com.YRH.IceCracker.G;

public class PlayHighScoreSprite extends CCSprite {
    CCSprite[] mDigit = new CCSprite[5];
	int score; float mfWidth, mfHeight;

	public PlayHighScoreSprite() {
		super("play_ic_best@2x.png");
		G.setScale(this); score = 0;
		
		CCSprite numberSet = CCSprite.sprite("play_best_number@2x.png");
		mfWidth = numberSet.getContentSize().width / 10;
		mfHeight = numberSet.getContentSize().height;
		for (int i = 0; i < G.MAX_DIGIT; i++) {
			mDigit[i] = CCSprite.sprite(numberSet.getTexture(),
				CGRect.make(i * mfWidth, 0, mfWidth, mfHeight));
			mDigit[i].setVisible(false);
            mDigit[i].setPosition(
            	getContentSize().width / 2 + (G.PadVSPhoneX * (DIGIT_X + i * DIGIT_WIDTH)),
            	getContentSize().height / 2 + (G.PadVSPhoneY * DIGIT_Y));
			addChild(mDigit[i], enum_tag_Digit + i, enum_tag_Digit + i);
		}
		numberSet = null;
	}
	
	public void refresh() {
		int num;
		for(num=0; num<mDigit.length; num++)
			mDigit[num].setVisible(false);
		
		if (score >= 10000) {
			num = score / 10000;
			mDigit[0].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[0].setVisible(true);
			num = (score / 1000) % 10;
			mDigit[1].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[1].setVisible(true);
			num = (score / 100) % 10;
			mDigit[2].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[2].setVisible(true);
			num = (score / 10) % 10;
			mDigit[3].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[3].setVisible(true);
			num = score % 10;
			mDigit[4].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[4].setVisible(true);	
		} else if (score >= 1000) {
			num = score / 1000;
			mDigit[0].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[0].setVisible(true);
			num = (score / 100) % 10;
			mDigit[1].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[1].setVisible(true);
			num = (score / 10) % 10;
			mDigit[2].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[2].setVisible(true);
			num = score % 10;
			mDigit[3].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[3].setVisible(true);
		} else if (score >= 100) {
			num = score / 100;
			mDigit[0].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[0].setVisible(true);
			num = (score / 10) % 10;
			mDigit[1].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[1].setVisible(true);
			num = score % 10;
			mDigit[2].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[2].setVisible(true);
		} else if (score >= 10) {
			num = score / 10;
			mDigit[0].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[0].setVisible(true);
			num = score % 10;
			mDigit[1].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[1].setVisible(true);
		} else if (score >= 0) {
			num = score % 10;
			mDigit[0].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[0].setVisible(true);
		}
	}
	
	public void clearScore() {
		score = 0; refresh();
	}
	
	public void updateScore(int value) {
		if (value > score) {
			score = value; refresh();
		}
	}
	
	public void setScore(int value) {
		score = value; refresh();
	}
	
	public void settingOpacity(int value) {
		setOpacity(value);
		for (int i = 0; i < G.MAX_DIGIT; i++) 
			mDigit[i].setOpacity(value);
	}

	@Override public void onExit() {
	    for (int i = 0; i <mDigit.length; i++) {
	        mDigit[i].removeFromParentAndCleanup(true);
	        mDigit[i] = null;
	    } mDigit = null;
		super.onExit();
		System.gc();
	}


	private static final int enum_tag_Digit = 0;
	
	private static final int DIGIT_WIDTH    = 8;
	private static final int DIGIT_X        = 33;
	private static final int DIGIT_Y        = 1;
}