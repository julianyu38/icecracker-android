package com.YRH.Objects;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCFadeTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;
import org.cocos2d.utils.javolution.MathLib;

import com.YRH.IceCracker.G;
import com.YRH.Others.GameData;
import com.YRH.Others.GameSound;


public class PlayOptionSprite extends CCSprite {
    CCSprite mbtnEffectSprite;
    CCSprite mbtnSoundSprite;
    CCSprite mbtnVibrationSprite;
    CCSprite mbtnExitSprite;
    CCSprite mPlayOptionLabelSprite;
    
	boolean _isPressed;
	boolean _isiPhone;
	int _btnSelected;
	float _appearTime;

	public PlayOptionSprite() {
		super("panel_background@2x.png"); G.setScale(this);
		setVisible(false); _isPressed = false;
		
		float halfSelfW = getContentSize().width / 2;
		float halfSelfH = getContentSize().height / 2;
		
        mbtnEffectSprite = CCSprite.sprite(GameData.getEffect() ?
        	"panel_option_sound_select@2x.png" :
        	"panel_option_sound_unselect@2x.png");
        mbtnEffectSprite.setPosition(
        	halfSelfW + (G.PadVSPhoneX * BTN_SOUND_X),
        	halfSelfH + (G.PadVSPhoneY * BTN_SOUND_Y));
		addChild(mbtnEffectSprite, 0);
		
        mbtnSoundSprite = CCSprite.sprite(GameData.getSound() ?
        	"panel_option_music_select@2x.png" :
        	"panel_option_music_unselect@2x.png");
        mbtnSoundSprite.setPosition(
        	halfSelfW + (G.PadVSPhoneX * BTN_MUSIC_X),
        	halfSelfH + (G.PadVSPhoneY * BTN_MUSIC_Y));
		addChild(mbtnSoundSprite, 1);
		
        mbtnVibrationSprite = CCSprite.sprite(GameData.getVibration() ?
        	"panel_option_vibration_select@2x.png" : 
        	"panel_option_vibration_unselect@2x.png");
        mbtnVibrationSprite.setPosition(
        	halfSelfW + (G.PadVSPhoneX * BTN_VIBRATION_X),
        	halfSelfH + (G.PadVSPhoneY * BTN_VIBRATION_Y));
		addChild(mbtnVibrationSprite, 2);
		
		mbtnExitSprite = CCSprite.sprite("panel_btn_back_normal@2x.png");
        mbtnExitSprite.setPosition(
        	halfSelfW + (G.PadVSPhoneX * BTN_EXIT_X),
        	halfSelfH + (G.PadVSPhoneY * BTN_EXIT_Y));
		addChild(mbtnExitSprite, 3);
		
		mPlayOptionLabelSprite = CCSprite.sprite("panel_title_option@2x.png");
        mPlayOptionLabelSprite.setPosition(
        	halfSelfW + (G.PadVSPhoneX * PLAY_OPTION_LABEL_X),
        	halfSelfH + (G.PadVSPhoneY * PLAY_OPTION_LABEL_Y));
		addChild(mPlayOptionLabelSprite, 4);
		
		setOpacity(0);
		mbtnEffectSprite.setOpacity(0);
		mbtnSoundSprite.setOpacity(0);
		mbtnVibrationSprite.setOpacity(0);
		mbtnExitSprite.setOpacity(0);
		mPlayOptionLabelSprite.setOpacity(0);
	}
	
	public int getClick(CGPoint pos) {
		float dx, dy;
		float x = pos.x; float y = pos.y;
		dx = x - mbtnEffectSprite.getPosition().x/*BTN_SOUND_X*/;
		dy = y - mbtnEffectSprite.getPosition().y/*BTN_SOUND_Y*/;
		if (-BTN_SOUND_WIDTH / 2 < dx && dx < BTN_SOUND_WIDTH / 2 &&
			-BTN_SOUND_HEIGHT / 2 < dy && dy < BTN_SOUND_HEIGHT / 2)
	        return enum_click_sound;
	
		dx = x - mbtnSoundSprite.getPosition().x/*BTN_MUSIC_X*/;
		dy = y - mbtnSoundSprite.getPosition().y/*BTN_MUSIC_Y*/;
		if (-BTN_MUSIC_WIDTH / 2 < dx && dx < BTN_MUSIC_WIDTH / 2 &&
			-BTN_MUSIC_HEIGHT / 2 < dy && dy < BTN_MUSIC_HEIGHT / 2)
	        return enum_click_music;
		
		dx = x - mbtnVibrationSprite.getPosition().x/*BTN_VIBRATION_X*/;
		dy = y - mbtnVibrationSprite.getPosition().y/*BTN_VIBRATION_Y*/;
		if (-BTN_VIBRATION_WIDTH / 2 < dx && dx < BTN_VIBRATION_WIDTH / 2 &&
			-BTN_VIBRATION_HEIGHT / 2 < dy && dy < BTN_VIBRATION_HEIGHT / 2)
	        return enum_click_vibration;
		
		dx = x - mbtnExitSprite.getPosition().x/*BTN_EXIT_X*/;
		dy = y - mbtnExitSprite.getPosition().y/*BTN_EXIT_Y*/;
		if (MathLib.sqrt(dx * dx + dy * dy) < BTN_EXIT_R)
	        return enum_click_exit;
		
		return enum_click_none;
	}
	
	public void touchesBegan(float x, float y) {
		CGPoint pos = convertToNodeSpace(x, y);
		int touchClick = getClick(pos);
		if (touchClick == enum_click_none) return;
		if (touchClick == enum_click_exit) _btnSelected = enum_click_exit;
		
		switch (touchClick) {
			case enum_click_sound:	
				if (GameData.getEffect()) {
					GameData.setEffect(false);
					CCSprite tmp = CCSprite.sprite("panel_option_sound_unselect@2x.png");
					mbtnEffectSprite.setTexture(tmp.getTexture());
					mbtnEffectSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
				} else {
					GameData.setEffect(true);
					CCSprite tmp = CCSprite.sprite("panel_option_sound_select@2x.png");
					mbtnEffectSprite.setTexture(tmp.getTexture());
					mbtnEffectSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
				}
				break;
			case enum_click_music:	
				if (GameData.getSound()) {
					GameData.setSound(false);
					GameSound.stopBG();
					CCSprite tmp = CCSprite.sprite("panel_option_music_unselect@2x.png");
					mbtnSoundSprite.setTexture(tmp.getTexture());
					mbtnSoundSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
				} else {
					GameData.setSound(true);
	                GameSound.playGameBG();
					CCSprite tmp = CCSprite.sprite("panel_option_music_select@2x.png");
					mbtnSoundSprite.setTexture(tmp.getTexture());
					mbtnSoundSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
				}
				break;
			case enum_click_vibration:
				if (GameData.getVibration()) {
					GameData.setVibration(false);
					CCSprite tmp = CCSprite.sprite("panel_option_vibration_unselect@2x.png");
					mbtnVibrationSprite.setTexture(tmp.getTexture());
					mbtnVibrationSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
				} else {
					GameData.setVibration(true);
					GameSound.vib(GameSound.VIB_BUTTON);
					CCSprite tmp = CCSprite.sprite("panel_option_vibration_select@2x.png");
					mbtnVibrationSprite.setTexture(tmp.getTexture());
					mbtnVibrationSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
				}
				break;
			case enum_click_exit:	
				mbtnExitSprite.setOpacity(G.PRESS_OPACITY);
				_isPressed = true;
				break;
			default: break;
		}
		GameSound.effectBtnClick();
		System.gc();
	}
	
	public void touchesMoved(float x, float y) {
		if (!_isPressed) return;
		CGPoint pos = convertToNodeSpace(x, y);
		int touchClick = getClick(pos);
		if (touchClick != enum_click_exit) touchClick = enum_click_none;
		if (touchClick == _btnSelected) return;
		if (touchClick != enum_click_none) GameSound.effectBtnClick();
		
		switch (_btnSelected) {
		case enum_click_exit:		
            mbtnExitSprite.setOpacity(G.RELEASE_OPACITY);		
            break;
		default: break;
		}
		_btnSelected = enum_click_exit;
        mbtnExitSprite.setOpacity(G.PRESS_OPACITY);		
	}
	
	public int touchesEnded(float x, float y) {
		if (!_isPressed) return enum_play_option_none;
		_isPressed = false;
		CGPoint pos = convertToNodeSpace(x, y);
		int touchClick = getClick(pos);
		switch (touchClick) {
		case enum_click_exit:		
            mbtnExitSprite.setOpacity(G.RELEASE_OPACITY);		
            return enum_play_option_exit;
		default: break;
		}
		return enum_play_option_none;
	}
	
	public void appearSelf() {
		runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearEffect() {
		mbtnEffectSprite.runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));	
	}
	
	public void appearSound() {
		mbtnSoundSprite.runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));	
	}
	
	public void appearVibration() {
		mbtnVibrationSprite.runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));	
	}
	
	public void appearExit() {
		mbtnExitSprite.runAction(CCFadeTo.action(_appearTime, G.RELEASE_OPACITY));	
	}
	
	public void appearLabel() {
		mPlayOptionLabelSprite.runAction(
			CCFadeTo.action(_appearTime, G.PRESS_OPACITY));	
	}
	
	public void appearSprite(float duration) {
		_appearTime = duration;
		runAction(CCSequence.actions(
			 CCCallFunc.action(this, "appearSelf"),
			 CCCallFunc.action(this, "appearLabel"),
			 CCCallFunc.action(this, "appearExit"),
			 CCCallFunc.action(this, "appearEffect"),
			 CCCallFunc.action(this, "appearSound"),
			 CCCallFunc.action(this, "appearVibration")));
	}
	
	public void disappearSelf() {
		runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearEffect() {
		mbtnEffectSprite.runAction(CCFadeTo.action(_appearTime, 0));	
	}
	
	public void disappearSound() {
		mbtnSoundSprite.runAction(CCFadeTo.action(_appearTime, 0));	
	}
	
	public void disappearVibration() {
		mbtnVibrationSprite.runAction(CCFadeTo.action(_appearTime, 0));	
	}
	
	public void disappearExit() {
		mbtnExitSprite.runAction(CCFadeTo.action(_appearTime, 0));	
	}
	
	public void disappearLabel() {
		mPlayOptionLabelSprite.runAction(CCFadeTo.action(_appearTime, 0));	
	}
	
	public void disappearSprite(float duration) {
		_appearTime = duration;
		runAction(CCSequence.actions(
			 CCCallFunc.action(this, "disappearExit"),
			 CCCallFunc.action(this, "disappearLabel"),
			 CCCallFunc.action(this, "disappearEffect"),
			 CCCallFunc.action(this, "disappearSound"),
			 CCCallFunc.action(this, "disappearVibration"),
			 CCCallFunc.action(this, "disappearSelf")));
	}
	
	private static final int BTN_SOUND_X		= 0;
	private static final int BTN_SOUND_Y		= 15;
	private static final int BTN_SOUND_WIDTH	= 110;
	private static final int BTN_SOUND_HEIGHT	= 27;
	
	private static final int BTN_MUSIC_X		= 0;
	private static final int BTN_MUSIC_Y		= -35;
	private static final int BTN_MUSIC_WIDTH	= 100;
	private static final int BTN_MUSIC_HEIGHT	= 29;
	
	private static final int BTN_VIBRATION_X	= 0;
	private static final int BTN_VIBRATION_Y	= -85;
	private static final int BTN_VIBRATION_WIDTH= 168;
	private static final int BTN_VIBRATION_HEIGHT= 29;
	
	private static final int BTN_EXIT_X			= 0;
	private static final int BTN_EXIT_Y			= -138;
	private static final int BTN_EXIT_R			= 40;
	
	private static final int PLAY_OPTION_LABEL_X= 0;
	private static final int PLAY_OPTION_LABEL_Y= 58;
	
	private static final int enum_click_none 	= 0;
	private static final int enum_click_sound 	= 1;
	private static final int enum_click_music 	= 2;
	private static final int enum_click_vibration= 3;
	private static final int enum_click_exit 	= 4;

	private static final int enum_play_option_none= 0;
	public static final int enum_play_option_exit = 1;
}