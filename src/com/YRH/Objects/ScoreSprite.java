package com.YRH.Objects;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCFadeTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;

import com.YRH.IceCracker.G;
import com.YRH.Others.GameSound;

public class ScoreSprite extends CCSprite {
    CCSprite _btnReplaySprite;
    CCSprite _btnExitSprite;
    CCSprite _scorePanelSprite;
    CCSprite _scoreLabelSprite;
    CCSprite[] _scoreNum = new CCSprite[5];
    CCSprite[] _star = new CCSprite[5];
    
	boolean _isPressed;
	int _btnSelected;
	int _score;
	float _appearTime;

	public ScoreSprite() {
		super("panel_background@2x.png");
		G.setScale(this); setVisible(false);
		_score = 0; _isPressed = false;

		float halfSelfW = getContentSize().width / 2;
		float halfSelfH = getContentSize().height / 2;

		_btnReplaySprite = CCSprite.sprite("result_menu_replay@2x.png");
        _btnReplaySprite.setPosition(
        	halfSelfW + G.PadVSPhoneX * BTN_REPLAY_X,
        	halfSelfH + G.PadVSPhoneY * BTN_REPLAY_Y);
		addChild(_btnReplaySprite, 0);
		
		_btnExitSprite = CCSprite.sprite("result_menu_quit@2x.png");
        _btnExitSprite.setPosition(
        	halfSelfW + G.PadVSPhoneX * BTN_EXIT_X,
        	halfSelfH + G.PadVSPhoneY * BTN_EXIT_Y);
		addChild(_btnExitSprite, 1);
		
		_scorePanelSprite = CCSprite.sprite("result_view@2x.png");
        _scorePanelSprite.setPosition(
        	halfSelfW + G.PadVSPhoneX * SCORE_PANEL_X,
        	halfSelfH + G.PadVSPhoneY * SCORE_PANEL_Y);
		addChild(_scorePanelSprite, 2);
		
		_scoreLabelSprite = CCSprite.sprite("result_score@2x.png");
        _scoreLabelSprite.setPosition(
        	halfSelfW + G.PadVSPhoneX * SCORE_LABEL_X,
        	halfSelfH + G.PadVSPhoneY * SCORE_LABEL_Y);
		addChild(_scoreLabelSprite, 3);
		
		for (int i = 0; i < MAX_SCORE_NUM; i++) {
			_scoreNum[i] = CCSprite.sprite("result_score_number@2x.png");
			_scoreNum[i].setTextureRect(0, 0,
				_scoreNum[i].getContentSize().width / 10,
				_scoreNum[i].getContentSize().height, false);
            _scoreNum[i].setPosition(
            	halfSelfW + (G.PadVSPhoneX * (SCORE_NUMBER_X + i * SCORE_NUMBER_SPAN)),
            	halfSelfH + G.PadVSPhoneY * SCORE_NUMBER_Y);
			_scoreNum[i].setOpacity(0);
			addChild(_scoreNum[i], 10 + i);
		}
		
		for (int i = 0; i < MAX_STAR_NUM; i++) {
			_star[i] = CCSprite.sprite("result_ranking_off@2x.png");
            _star[i].setPosition(
            	halfSelfW + G.PadVSPhoneX * (STAR_X + i * STAR_SPAN),
            	halfSelfH + G.PadVSPhoneY * STAR_Y);
			_star[i].setOpacity(0);
			addChild(_star[i], 20 + i);
		}
		
		setOpacity(0);
		_btnReplaySprite.setOpacity(0);
		_btnExitSprite.setOpacity(0);
		_scorePanelSprite.setOpacity(0);
        _scorePanelSprite.setVisible(false);
		_scoreLabelSprite.setOpacity(0);
		refresh();
	}
	
	public int getClick(CGPoint pos) {
		float x = pos.x; float y = pos.y; 
		float dx, dy;
		dx = x - _btnReplaySprite.getPosition().x/*BTN_REPLAY_X*/;
		dy = y - _btnReplaySprite.getPosition().y/*BTN_REPLAY_Y*/;
		if (-BTN_REPLAY_WIDTH / 2 < dx && dx < BTN_REPLAY_WIDTH / 2 &&
			-BTN_REPLAY_HEIGHT / 2 < dy && dy < BTN_REPLAY_HEIGHT / 2)
	        return enum_click_replay;
		
		dx = x - _btnExitSprite.getPosition().x/*BTN_EXIT_X*/;
		dy = y - _btnExitSprite.getPosition().y/*BTN_EXIT_Y*/;
		if (-BTN_EXIT_WIDTH / 2 < dx && dx < BTN_EXIT_WIDTH / 2 &&
			-BTN_EXIT_HEIGHT / 2 < dy && dy < BTN_EXIT_HEIGHT / 2)
	        return enum_click_exit;
		
		return enum_click_none;
	}
	
	public void touchesBegan(float x, float y) {
		CGPoint pos = convertToNodeSpace(x, y);
		int touchClick = getClick(pos);
		if (touchClick == enum_click_none) return;
		
		GameSound.effectBtnClick();
		_btnSelected = touchClick;
		switch (_btnSelected) {
			case enum_click_replay:	
	            _btnReplaySprite.setOpacity(G.PRESS_OPACITY);	
	            break;
			case enum_click_exit:		
	            _btnExitSprite.setOpacity(G.PRESS_OPACITY);	
	            break;
			default:			
	            break;
		}
		_isPressed = true;
	}
	
	public void touchesMoved(float x, float y) {
		if (!_isPressed) return;
	
		CGPoint pos = convertToNodeSpace(x, y);
		int touchClick = getClick(pos);
		
		if (touchClick == _btnSelected) return;
		
		if (touchClick != enum_click_none)
	        GameSound.effectBtnClick();
		switch (_btnSelected) {
		case enum_click_replay:	
            _btnReplaySprite.setOpacity(G.RELEASE_OPACITY);	
            break;
		case enum_click_exit:		
            _btnExitSprite.setOpacity(G.RELEASE_OPACITY);		
            break;
		default:			
            break;
		}
		_btnSelected = touchClick;
		switch (_btnSelected) {
		case enum_click_replay:	
            _btnReplaySprite.setOpacity(G.PRESS_OPACITY);	
            break;
		case enum_click_exit:		
            _btnExitSprite.setOpacity(G.PRESS_OPACITY);		
            break;
		default:			
            break;
		}	
	}
	
	public int touchesEnded(float x, float y) {
		if (!_isPressed) return enum_score_none;
	
		_isPressed = false;
		CGPoint pos = convertToNodeSpace(x, y);
		int touchClick = getClick(pos);
		
		switch (touchClick) {
		case enum_click_replay:	
            _btnReplaySprite.setOpacity(G.RELEASE_OPACITY);	
            return enum_score_reply;
		case enum_click_exit:
            _btnExitSprite.setOpacity(G.RELEASE_OPACITY);		
            return enum_score_exit;
		default:			
            break;
		}
		return enum_score_none;
	}
	
	public void setScore(int value) {
		_score = value;
		refresh();
	}
	
	public void refresh() {
		int starnum = 0;
	    if (_score != 0) {
	        if (_score < 1000)  						starnum = 1;
	        else if (_score >= 1000 && _score < 3000 ) 	starnum = 2;
	        else if (_score >= 3000 && _score < 5000 ) 	starnum = 3;
	        else if (_score >= 5000 && _score < 10000 ) starnum = 4;
	        else if (_score >= 10000)  					starnum = 5;
	    }
	
		CCSprite tmpref = CCSprite.sprite("result_ranking_on@2x.png");
		for (int i = 0; i < starnum; i++) {
			_star[i].setTexture(tmpref.getTexture());
			_star[i].setTextureRect(tmpref.getTextureRect());
		} tmpref = null;
		tmpref = CCSprite.sprite("result_ranking_off@2x.png");
		for (int i = starnum; i < MAX_STAR_NUM; i++) {
			_star[i].setTexture(tmpref.getTexture());
			_star[i].setTextureRect(tmpref.getTextureRect());
		} tmpref = null;
		System.gc();
	    
		int num; final int fwidth = 52, fheight = 56;
	
		for(int i=0; i<_scoreNum.length; i++)
			_scoreNum[i].setVisible(false);
		if (_score >= 10000) {
			num = _score / 10000;
			_scoreNum[0].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[0].setVisible(true);
			num = (_score / 1000) % 10;
			_scoreNum[1].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[1].setVisible(true);
			num = (_score / 100) % 10;
			_scoreNum[2].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[2].setVisible(true);
			num = (_score / 10) % 10;
			_scoreNum[3].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[3].setVisible(true);
			num = _score % 10;
			_scoreNum[4].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[4].setVisible(true);	
		}
		else if (_score >= 1000) {
			num = _score / 1000;
			_scoreNum[0].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[0].setVisible(true);
			num = (_score / 100) % 10;
			_scoreNum[1].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[1].setVisible(true);
			num = (_score / 10) % 10;
			_scoreNum[2].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[2].setVisible(true);
			num = _score % 10;
			_scoreNum[3].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[3].setVisible(true);
		}
		else if (_score >= 100) {
			num = _score / 100;
			_scoreNum[0].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[0].setVisible(true);
			num = (_score / 10) % 10;
			_scoreNum[1].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[1].setVisible(true);
			num = _score % 10;
			_scoreNum[2].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[2].setVisible(true);
		}
		else if (_score >= 10) {
			num = _score / 10;
			_scoreNum[0].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[0].setVisible(true);
			num = _score % 10;
			_scoreNum[1].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[1].setVisible(true);
		}
		else if (_score >= 0) {
			num = _score % 10;
			_scoreNum[0].setTextureRect(num * fwidth, 0, fwidth, fheight, false);
			_scoreNum[0].setVisible(true);
		}
	}
	public void appearSelf() {
		runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearReplay() {
		_btnReplaySprite.runAction(CCFadeTo.action(_appearTime, G.RELEASE_OPACITY));	
	}
	
	public void appearExit() {
		_btnExitSprite.runAction(CCFadeTo.action(_appearTime, G.RELEASE_OPACITY));	
	}
	
	public void appearPanel() {
		_scorePanelSprite.runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));	
	}
	
	public void appearLabel() {
		_scoreLabelSprite.runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));	
	}
	
	public void appearStar1() {
		_star[0].runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearStar2() {
		_star[1].runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearStar3() {
		_star[2].runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearStar4() {
		_star[3].runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearStar5() {
		_star[4].runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearNum1() {
		_scoreNum[0].runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearNum2() {
		_scoreNum[1].runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearNum3() {
		_scoreNum[2].runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearNum4() {
		_scoreNum[3].runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearNum5() {
		_scoreNum[4].runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearSprite(float duration) {
		_appearTime = duration;
		runAction(CCSequence.actions(
			 CCCallFunc.action(this, "appearSelf"),
			 CCCallFunc.action(this, "appearLabel"),
			 CCCallFunc.action(this, "appearPanel"),
			 CCCallFunc.action(this, "appearReplay"),
			 CCCallFunc.action(this, "appearExit"),
			 CCCallFunc.action(this, "appearPanel"),
			 CCCallFunc.action(this, "appearLabel"),
			 CCCallFunc.action(this, "appearStar1"),
			 CCCallFunc.action(this, "appearStar2"),
			 CCCallFunc.action(this, "appearStar3"),
			 CCCallFunc.action(this, "appearStar4"),
			 CCCallFunc.action(this, "appearStar5"),
			 CCCallFunc.action(this, "appearNum1"),
			 CCCallFunc.action(this, "appearNum2"),
			 CCCallFunc.action(this, "appearNum3"),
			 CCCallFunc.action(this, "appearNum4"),
			 CCCallFunc.action(this, "appearNum5")));
	}
	
	public void disappearSelf() {
		runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearReplay() {
		_btnReplaySprite.runAction(CCFadeTo.action(_appearTime, 0));	
	}
	
	public void disappearExit() {
		_btnExitSprite.runAction(CCFadeTo.action(_appearTime, 0));	
	}
	
	public void disappearPanel() {
		_scorePanelSprite.runAction(CCFadeTo.action(_appearTime, 0));	
	}
	
	public void disappearLabel() {
		_scoreLabelSprite.runAction(CCFadeTo.action(_appearTime, 0));	
	}
	
	public void disappearStar1() {
		_star[0].runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearStar2() {
		_star[1].runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearStar3() {
		_star[2].runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearStar4() {
		_star[3].runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearStar5() {
		_star[4].runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearNum1() {
		_scoreNum[0].runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearNum2() {
		_scoreNum[1].runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearNum3() {
		_scoreNum[2].runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearNum4() {
		_scoreNum[3].runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearNum5() {
		_scoreNum[4].runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearSprite(float duration) {
		_appearTime = duration;
		runAction(CCSequence.actions(
			 CCCallFunc.action(this, "disappearStar1"),
			 CCCallFunc.action(this, "disappearStar2"),
			 CCCallFunc.action(this, "disappearStar3"),
			 CCCallFunc.action(this, "disappearStar4"),
			 CCCallFunc.action(this, "disappearStar5"),
			 CCCallFunc.action(this, "disappearNum1"),
			 CCCallFunc.action(this, "disappearNum2"),
			 CCCallFunc.action(this, "disappearNum3"),
			 CCCallFunc.action(this, "disappearNum4"),
			 CCCallFunc.action(this, "disappearNum5"),
			 CCCallFunc.action(this, "disappearReplay"),
			 CCCallFunc.action(this, "disappearExit"),
			 CCCallFunc.action(this, "disappearPanel"),
			 CCCallFunc.action(this, "disappearLabel"),
			 CCCallFunc.action(this, "disappearSelf"),
			 CCCallFunc.action(this, "disappearPanel"),
			 CCCallFunc.action(this, "disappearLabel")));
	}
	
	@Override public void onExit() {
	    for (int i = 0; i < _scoreNum.length; i++) {
	        _scoreNum[i].removeFromParentAndCleanup(true);
	        _scoreNum[i] = null;
	    } _scoreNum = null;
	    for (int i = 0; i < _star.length; i++) {
	        _star[i].removeFromParentAndCleanup(true);
	        _star[i] = null;
	    } _star = null;

		super.onExit();
		System.gc();
	}



	private static final int BTN_REPLAY_X		= 6;
	private static final int BTN_REPLAY_Y		= -35;
	private static final int BTN_REPLAY_WIDTH	= 182;
	private static final int BTN_REPLAY_HEIGHT	= 30;

	private static final int BTN_EXIT_X			= 6;
	private static final int BTN_EXIT_Y			= -83;
	private static final int BTN_EXIT_WIDTH		= 182;
	private static final int BTN_EXIT_HEIGHT	= 30;

	private static final int STAR_X       		= -84;
	private static final int STAR_Y  			= 63;
	private static final int STAR_SPAN  		= 45;

	private static final int SCORE_PANEL_X		= 6;
	private static final int SCORE_PANEL_Y		= 15;

	private static final int SCORE_LABEL_X		= -57;
	private static final int SCORE_LABEL_Y		= SCORE_PANEL_Y;

	private static final int SCORE_NUMBER_X		= 8;
	private static final int SCORE_NUMBER_Y		= SCORE_PANEL_Y;
	private static final int SCORE_NUMBER_SPAN	= 25;

	private static final int MAX_SCORE_NUM		= 5;
	private static final int MAX_STAR_NUM		= 5;

	private static final int enum_click_none 		= 0;
	private static final int enum_click_replay 		= 1;
	private static final int enum_click_exit 		= 2;
	
	private static final int enum_score_none 		= 0;
	public static final int enum_score_reply 		= 1;
	public static final int enum_score_exit 		= 2;
}