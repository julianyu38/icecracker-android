package com.YRH.Objects;

import org.cocos2d.nodes.CCSprite;
import org.cocos2d.nodes.CCTextureCache;
import org.cocos2d.opengl.CCTexture2D;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;
import org.cocos2d.utils.javolution.MathLib;

import com.YRH.IceCracker.G;

public class KnifeSprite extends CCSprite {
	public boolean isActive;
	float _appearTime;
	float _visibleTime;
	int _knifeColor;

	public KnifeSprite() {
		super("play_knife_0@2x.png");
		G.setScale(this);
		isActive = false;
		setOpacity(0);
	}
	
	public void initKnife() {
		setOpacity(0);
		isActive = false;
	}
	
	public boolean splitAction(float posX1, float posY1, 
		float posX2, float posY2, int selColor) {
		isActive = true; setOpacity(0);
		_visibleTime = APPEAR_TIME;
		_knifeColor = selColor;
		CCTexture2D tmp = CCTextureCache.sharedTextureCache().addImage(
			String.format("play_knife_%d@2x.png", selColor));
		setTexture(tmp);
		setTextureRect(CGRect.make(CGPoint.zero(), tmp.getContentSize()));
		tmp = null;
		float alpha = (float) MathLib.atan2(posY2 - posY1, posX2 - posX1);
		setRotation(-(float) MathLib.toDegrees(alpha));
		setPosition((posX1 + posX2) / 2, (posY1 + posY2) / 2);
		setOpacity(255);
		schedule("update", G.TIME_TICKS);
		_appearTime = 0;
		return false;
	}
	
	public boolean appearAction(float posX1, float posY1, float posX2, 
		float posY2, int selColor, boolean soundEffect) {
		float length = (float) MathLib.sqrt((posX1 - posX2) * (posX1 - posX2) +
			(posY1 - posY2) * (posY1 - posY2));
		if (length < 20) return false;
		
		isActive = true; setOpacity(0); _visibleTime = 0.3f;
		_knifeColor = selColor;
//		switch (selColor) {
//		case enum_knife_0:
//			tmp = CCSprite.sprite("play_knife_0@2x.png");
//			setTexture(tmp.getTexture());
//			setTextureRect(tmp.getTextureRect());
//			break;
//		case enum_knife_1:
//			tmp = CCSprite.sprite("play_knife_1@2x.png");
//			setTexture(tmp.getTexture());
//			setTextureRect(tmp.getTextureRect());
//			break;
//		case enum_knife_2:
//			tmp = CCSprite.sprite("play_knife_2@2x.png");
//			setTexture(tmp.getTexture());
//			setTextureRect(tmp.getTextureRect());
//			break;
//		case enum_knife_3:
//			tmp = CCSprite.sprite("play_knife_3@2x.png");
//			setTexture(tmp.getTexture());
//			setTextureRect(tmp.getTextureRect());
//			break;
//		default:	return false;
//		}
		
		CCTexture2D tmp = CCTextureCache.sharedTextureCache().addImage(
			String.format("play_knife_%d@2x.png", selColor));
		setTexture(tmp);
		setTextureRect(CGRect.make(CGPoint.zero(), tmp.getContentSize()));
		tmp = null;	
		setScaleX(length / (float)(getContentSize().width));
		setScaleY(MathLib.max(getScaleX(), KNIFE_MIN_SCALEY));
		float alpha = (float) MathLib.atan2(posY2 - posY1, posX2 - posX1);
		setRotation(-(float) MathLib.toDegrees(alpha));
		setPosition((posX1 + posX2) / 2, (posY1 + posY2) / 2);
		setOpacity(255); schedule("update", G.TIME_TICKS);
		_appearTime = 0;
		return true;
	}
	
	public void update(float deltaTime) {
		_appearTime += deltaTime;
		if (_appearTime > _visibleTime) {
			setOpacity(0);
			unschedule("update");
			isActive = false;
			return;
		}
		int opp = (int) (255 - _appearTime * 255 / _visibleTime);
		opp = MathLib.max(0, opp);
		setOpacity((int) opp);
	}
	
	public static final int enum_knife_0 = 0;
	public static final int enum_knife_1 = 1;
	public static final int enum_knife_2 = 2;
	public static final int enum_knife_3 = 3;
	
	private static final int 	APPEAR_TIME			= 1;
	private static final float 	KNIFE_MIN_SCALEY	= 0.8f;

}