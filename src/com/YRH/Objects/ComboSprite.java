package com.YRH.Objects;

import org.cocos2d.nodes.CCSprite;
import org.cocos2d.utils.javolution.MathLib;

import com.YRH.IceCracker.G;
import com.YRH.Scenes.PlayScene;

public class ComboSprite extends CCSprite {
	CCSprite[] _prizeSprite = new CCSprite[20];
	public boolean isActive;
	float _appearTime;
	int _comboNumber;
	int _randomNum;

	public ComboSprite() {
		super("play_fire.png"); G.setScale(this);
		setOpacity(0);
		isActive = false;
		
		for (int i=0; i<20; i++) {
			_prizeSprite[i] = CCSprite.sprite(String.format("%s.png", Prizes_Name[i]));
			_prizeSprite[i].setTextureRect(0, 0, PRIZE_WIDTH, PRIZE_HEIGHT, false);
			_prizeSprite[i].setPosition(
				getContentSize().width / 2 + PRIZE_X,
				getContentSize().height / 2 + PRIZE_Y);
			addChild(_prizeSprite[i], enum_tag_prize+i, enum_tag_prize+i);
			_prizeSprite[i].setOpacity(0);
		}
	}
	
	public void initEffect() {
		setOpacity(0);
		for (int i=0; i<20; i++)
			_prizeSprite[i].setOpacity(0);
		isActive = false;
	}
	
	public boolean appearEffect(int number, float posX, float posY) {
		isActive = true;
		_comboNumber = MathLib.min(90, number);
		setPosition(posX, posY);
		_randomNum = MathLib.random(0, 19);
		_prizeSprite[_randomNum].setOpacity(255);
		setOpacity(255);
		setVisible(true);
		schedule("update", G.TIME_TICKS);
		_appearTime = 0;
		return false;
	}
	
	public void update(float deltaTime) {
		_appearTime += deltaTime;
		if (_appearTime > APPEAR_TIME) {
			isActive = false;
			setVisible(false);
			setOpacity(0);
			_prizeSprite[_randomNum].setOpacity(0);
			unschedule("update");
			((PlayScene) getParent()).addScore(_comboNumber);
			return;
		}
		int opp = (int) MathLib.max(255 - _appearTime * 255 / APPEAR_TIME, 0);
		setOpacity(opp);
		_prizeSprite[_randomNum].setOpacity(opp);
	}

	
	@Override public void onExit() {
		for (int i=0; i<_prizeSprite.length; i++) {
			_prizeSprite[i].removeFromParentAndCleanup(true);
			_prizeSprite[i] = null;
		}
		_prizeSprite = null;
		super.onExit();
		System.gc();
	}


	private static final int enum_tag_prize = 0;
	private static final int APPEAR_TIME 	= 2;

	private static final int PRIZE_WIDTH 	= 140;
	private static final int PRIZE_HEIGHT	= 140;

	private static final int PRIZE_X		= 50;
	private static final int PRIZE_Y		= 0;

	final String[] Prizes_Name = {
		"prize_01", "prize_02", "prize_03", "prize_04",
		"prize_05", "prize_06", "prize_07", "prize_08",
		"prize_09", "prize_10", "prize_11", "prize_12",
		"prize_13", "prize_14", "prize_15", "prize_16",
		"prize_17", "prize_18", "prize_19", "prize_20",
	};
}