package com.YRH.Objects;

import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGRect;
import org.cocos2d.utils.javolution.MathLib;

import com.YRH.IceCracker.G;

public class PlayScoreSprite extends CCSprite {
    CCSprite mIceSprite; int score;
    CCSprite[] mDigit = new CCSprite[5];

    float mfWidth, mfHeight;
	
	public PlayScoreSprite() {
		super("play_count_number@2x.png"); score = 0; G.setScale(this);
		CCSprite numberSet = CCSprite.sprite("play_count_number@2x.png");
		mfWidth = (numberSet.getContentSize().width) / 11;
		mfHeight = numberSet.getContentSize().height;
        setTextureRect(10 * mfWidth, G._getPosY(3), mfWidth, mfHeight, false);
		
		mIceSprite = CCSprite.sprite("play_ic_count@2x.png");
        mIceSprite.setPosition(
        	mfWidth / 2 + (G.PadVSPhoneX * ICE_X),
        	mfHeight / 2 + (G.PadVSPhoneY * ICE_Y));
		addChild(mIceSprite, enum_tag_ice, enum_tag_ice);
		
		for (int i = 0; i < G.MAX_DIGIT; i++) {
			mDigit[i] = CCSprite.sprite(
				numberSet.getTexture(),
				CGRect.make(i * mfWidth, 0, mfWidth, mfHeight));
			mDigit[i].setVisible(false);
            mDigit[i].setPosition(
            	getContentSize().width / 2 + (G.PadVSPhoneX * (DIGIT_X + i * DIGIT_WIDTH)),
            	getContentSize().height / 2 + (G.PadVSPhoneY * DIGIT_Y));
			addChild(mDigit[i], enum_tag_digit + i, enum_tag_digit + i);
		}
		numberSet = null;
	}
	
	public void refresh() {
		int num;
		mDigit[0].setVisible(false);
		mDigit[1].setVisible(false);
		mDigit[2].setVisible(false);
		mDigit[3].setVisible(false);
		mDigit[4].setVisible(false);
		if (score >= 10000) {
			num = score / 10000;
			mDigit[0].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[0].setVisible(true);
			num = (score / 1000) % 10;
			mDigit[1].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[1].setVisible(true);
			num = (score / 100) % 10;
			mDigit[2].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[2].setVisible(true);
			num = (score / 10) % 10;
			mDigit[3].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[3].setVisible(true);
			num = score % 10;
			mDigit[4].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[4].setVisible(true);	
		} else if (score >= 1000) {
			num = score / 1000;
			mDigit[0].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[0].setVisible(true);
			num = (score / 100) % 10;
			mDigit[1].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[1].setVisible(true);
			num = (score / 10) % 10;
			mDigit[2].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[2].setVisible(true);
			num = score % 10;
			mDigit[3].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[3].setVisible(true);
		} else if (score >= 100) {
			num = score / 100;
			mDigit[0].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[0].setVisible(true);
			num = (score / 10) % 10;
			mDigit[1].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[1].setVisible(true);
			num = score % 10;
			mDigit[2].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[2].setVisible(true);
		} else if (score >= 10) {
			num = score / 10;
			mDigit[0].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[0].setVisible(true);
			num = score % 10;
			mDigit[1].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[1].setVisible(true);
		} else if (score >= 0) {
			num = score % 10;
			mDigit[0].setTextureRect(num * mfWidth, 0, mfWidth, mfHeight, false);
			mDigit[0].setVisible(true);
		}	
	}
	
	public void clearScore() {
		score = 0;
		refresh();
	}
	
	public void addScore(int addValue) {
		if (addValue == 0) return; 
		score = MathLib.max(score+addValue, 0);
		refresh();
	}
	
	public void settingOpacity(int value) {
		setOpacity(value);
		mIceSprite.setOpacity(value);
		for (int i = 0; i < G.MAX_DIGIT; i++)
			mDigit[i].setOpacity(value);
	}

	@Override public void onExit() {
	    for (int i = 0; i <mDigit.length; i++) {
	        mDigit[i].removeFromParentAndCleanup(true);
	        mDigit[i] = null;
	    }
	    mDigit = null;
		super.onExit();
	}
	
	private static final int enum_tag_ice 	= 0;
	private static final int enum_tag_digit = 1;
	
	private static final int ICE_X			= -20;
	private static final int ICE_Y      	= 3;
	
	private static final int DIGIT_WIDTH	= 14;
	private static final int DIGIT_X    	= 24;
	private static final int DIGIT_Y    	= 3;
}