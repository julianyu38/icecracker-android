package com.YRH.Objects;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCFadeTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGPoint;

import com.YRH.IceCracker.G;
import com.YRH.Others.GameSound;


public class PauseSprite extends CCSprite {
    CCSprite _btnContinueSprite;
    CCSprite _btnOptionSprite;
    CCSprite _btnExitSprite;
    CCSprite _pauseLabelSprite;
    
	boolean _isPressed;
	int _btnSelected;
	float _appearTime;
	
	public PauseSprite() {
		super("panel_background@2x.png");
		G.setScale(this);
		setVisible(false); _isPressed = false;
		float halfSelfW = getContentSize().width / 2;
		float halfSelfH = getContentSize().height / 2;
		
		_btnContinueSprite = CCSprite.sprite("panel_pause_continue_normal@2x.png");
        _btnContinueSprite.setPosition(
    		halfSelfW + (G.PadVSPhoneX * BTN_CONTINUE_X),
        	halfSelfH + (G.PadVSPhoneY * BTN_CONTINUE_Y));
		addChild(_btnContinueSprite, 0);
		
		_btnOptionSprite = CCSprite.sprite("panel_pause_options_normal@2x.png");
        _btnOptionSprite.setPosition(
        	halfSelfW + (G.PadVSPhoneX * BTN_OPTION_X),
        	halfSelfH + (G.PadVSPhoneY * BTN_OPTION_Y));
		addChild(_btnOptionSprite, 1);
		
		_btnExitSprite = CCSprite.sprite("panel_pause_mainmenu_normal@2x.png");
        _btnExitSprite.setPosition(
        	halfSelfW + (G.PadVSPhoneX * BTN_EXIT_X),
        	halfSelfH + (G.PadVSPhoneY * BTN_EXIT_Y));
		addChild(_btnExitSprite, 2);
		
		_pauseLabelSprite = CCSprite.sprite("panel_title_pause@2x.png");
        _pauseLabelSprite.setPosition(
        	halfSelfW + (G.PadVSPhoneX * PAUSE_LABEL_X),
        	halfSelfH + (G.PadVSPhoneY * PAUSE_LABEL_Y));
		addChild(_pauseLabelSprite, 3);
		
		setOpacity(0);
		_btnContinueSprite.setOpacity(0);
		_btnOptionSprite.setOpacity(0);
		_btnExitSprite.setOpacity(0);
		_pauseLabelSprite.setOpacity(0);
	}
	
	public int getClick(CGPoint pos) {
		float x = pos.x; float y = pos.y;
		float dx, dy;
		dx = x - _btnContinueSprite.getPosition().x/*BTN_CONTINUE_X*/;
		dy = y - _btnContinueSprite.getPosition().y/*BTN_CONTINUE_Y*/;
		if (-BTN_CONTINUE_WIDTH / 2 < dx && dx < BTN_CONTINUE_WIDTH / 2 &&
			-BTN_CONTINUE_HEIGHT / 2 < dy&& dy < BTN_CONTINUE_HEIGHT / 2)
	        return enum_click_continue;
		
		dx = x - _btnOptionSprite.getPosition().x/*BTN_OPTION_X*/;
		dy = y - _btnOptionSprite.getPosition().y/*BTN_OPTION_Y*/;
		if (-BTN_OPTION_WIDTH / 2 < dx && dx < BTN_OPTION_WIDTH / 2 &&
			-BTN_OPTION_HEIGHT / 2 < dy && dy < BTN_OPTION_HEIGHT / 2)
	        return enum_click_option;
		
		dx = x - _btnExitSprite.getPosition().x/*BTN_EXIT_X*/;
		dy = y - _btnExitSprite.getPosition().y/*BTN_EXIT_Y*/;
		if (-BTN_EXIT_WIDTH / 2 < dx && dx < BTN_EXIT_WIDTH / 2 &&
			-BTN_EXIT_HEIGHT / 2 < dy && dy < BTN_EXIT_HEIGHT / 2)
	        return enum_click_exit;
		
		return enum_click_none;
	}
	
	public void touchesBegan(float x, float y) {
	    CCSprite tmp;
	    CGPoint pos = convertToNodeSpace(x, y);
		int touchClick = getClick(pos);
		if (touchClick == enum_click_none) return;
		GameSound.effectBtnClick();
		_btnSelected = touchClick;
		switch (_btnSelected) {
		case enum_click_continue:
            tmp = CCSprite.sprite("panel_pause_continue_press@2x.png");
            _btnContinueSprite.setTexture(tmp.getTexture());
            _btnContinueSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
            _btnContinueSprite.setOpacity(G.PRESS_OPACITY);
            break;
		case enum_click_option:	
            tmp = CCSprite.sprite("panel_pause_options_press@2x.png");
            _btnOptionSprite.setTexture(tmp.getTexture());
            _btnOptionSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
            _btnOptionSprite.setOpacity(G.PRESS_OPACITY);
            break;
		case enum_click_exit:		
            tmp = CCSprite.sprite("panel_pause_mainmenu_press@2x.png");
            _btnExitSprite.setTexture(tmp.getTexture());
            _btnExitSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
            _btnExitSprite.setOpacity(G.PRESS_OPACITY);
            break;
		default:			
            break;
		}
		_isPressed = true;
		System.gc();
	}
	
	public void touchesMoved(float x, float y) {
		if (!_isPressed) return;
	    CGPoint pos = convertToNodeSpace(x, y);
		int touchClick = getClick(pos);
		if (touchClick == _btnSelected) return;
		
		if (touchClick != enum_click_none) GameSound.effectBtnClick();
		switch (_btnSelected) {
		case enum_click_continue:	
            _btnContinueSprite.setOpacity(G.RELEASE_OPACITY);	
            break;
		case enum_click_option:	
            _btnOptionSprite.setOpacity(G.RELEASE_OPACITY);	
            break;
		case enum_click_exit:		
            _btnExitSprite.setOpacity(G.RELEASE_OPACITY);		
            break;
		default:			
            break;
		}
		_btnSelected = touchClick;
		switch (_btnSelected) {
		case enum_click_continue:
            _btnContinueSprite.setOpacity(G.PRESS_OPACITY);	
            break;
		case enum_click_option:	
            _btnOptionSprite.setOpacity(G.PRESS_OPACITY);		
            break;
		case enum_click_exit:		
            _btnExitSprite.setOpacity(G.PRESS_OPACITY);		
            break;
		default:			
            break;
		}
	}
	
	public int touchesEnded(float x, float y) {
	    CCSprite tmp;
		if (!_isPressed) return enum_pause_none;
		_isPressed = false;
	    CGPoint pos = convertToNodeSpace(x, y);
		int touchClick = getClick(pos);
		
		switch (touchClick) {
			case enum_click_continue:	
	            tmp = CCSprite.sprite("panel_pause_continue_normal@2x.png");
	            _btnContinueSprite.setTexture(tmp.getTexture());
	            _btnContinueSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
	            _btnContinueSprite.setOpacity(G.RELEASE_OPACITY);
	            return enum_pause_continue;
			case enum_click_option:
	            tmp = CCSprite.sprite("panel_pause_options_normal@2x.png");
	            _btnOptionSprite.setTexture(tmp.getTexture());
	            _btnOptionSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
	            _btnOptionSprite.setOpacity(G.RELEASE_OPACITY);
	            return enum_pause_option;
			case enum_click_exit:		
	            tmp = CCSprite.sprite("panel_pause_mainmenu_normal@2x.png");
	            _btnExitSprite.setTexture(tmp.getTexture());
	            _btnExitSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
	            _btnExitSprite.setOpacity(G.RELEASE_OPACITY);
	            return enum_pause_exit;
			default:			
	            break;
		}
		System.gc();
		return enum_pause_none;
	}
	
	public void appearSelf() {
		runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));
	}
	
	public void appearContinue() {
		_btnContinueSprite.runAction(CCFadeTo.action(_appearTime, G.RELEASE_OPACITY));	
	}
	
	public void appearOption() {
		_btnOptionSprite.runAction(CCFadeTo.action(_appearTime, G.RELEASE_OPACITY));	
	}
	
	public void appearExit() {
		_btnExitSprite.runAction(CCFadeTo.action(_appearTime, G.RELEASE_OPACITY));	
	}
	
	public void appearLabel() {
		_pauseLabelSprite.runAction(CCFadeTo.action(_appearTime, G.PRESS_OPACITY));	
	}
	
	public void appearSprite(float duration) {
		_appearTime = duration;
		runAction(CCSequence.actions(
			 CCCallFunc.action(this, "appearSelf"),
			 CCCallFunc.action(this, "appearLabel"),
			 CCCallFunc.action(this, "appearContinue"),
			 CCCallFunc.action(this, "appearOption"),
			 CCCallFunc.action(this, "appearExit")));
	}
	
	public void disappearSelf() {
		runAction(CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearContinue() {
		_btnContinueSprite.runAction(CCFadeTo.action(_appearTime, 0));	
	}
	
	public void disappearOption() {
		_btnOptionSprite.runAction(CCFadeTo.action(_appearTime, 0));	
	}
	
	public void disappearExit() {
		_btnExitSprite.runAction(CCFadeTo.action(_appearTime, 0));	
	}
	
	public void disappearLabel() {
		_pauseLabelSprite.runAction(
			CCFadeTo.action(_appearTime, 0));
	}
	
	public void disappearSprite(float duration) {
		_appearTime = duration;
		runAction(CCSequence.actions(
			 CCCallFunc.action(this, "disappearLabel"),
			 CCCallFunc.action(this, "disappearContinue"),
			 CCCallFunc.action(this, "disappearOption"),
			 CCCallFunc.action(this, "disappearExit"),
			 CCCallFunc.action(this, "disappearSelf")));
	}

	private static final int BTN_CONTINUE_X		= 0;
	private static final int BTN_CONTINUE_Y		= 15;
	private static final int BTN_CONTINUE_WIDTH	= 154;
	private static final int BTN_CONTINUE_HEIGHT= 30;
	
	private static final int BTN_OPTION_X		= 0;
	private static final int BTN_OPTION_Y		= -35;
	private static final int BTN_OPTION_WIDTH	= 136;
	private static final int BTN_OPTION_HEIGHT	= 30;
	
	private static final int BTN_EXIT_X			= 0;
	private static final int BTN_EXIT_Y			= -83;
	private static final int BTN_EXIT_WIDTH		= 194;
	private static final int BTN_EXIT_HEIGHT	= 30;
	
	private static final int PAUSE_LABEL_X		= 0;
	private static final int PAUSE_LABEL_Y		= 65;

	private static final int enum_click_none 	= 0;
	private static final int enum_click_continue= 1;
	private static final int enum_click_option 	= 2;
	private static final int enum_click_exit 	= 3;

	public static final int enum_pause_none		= 0;
	public static final int enum_pause_continue	= 1;
	public static final int enum_pause_option 	= 2;
	public static final int enum_pause_exit 	= 3;
}
