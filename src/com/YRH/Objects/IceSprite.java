package com.YRH.Objects;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.utils.javolution.MathLib;

import com.YRH.IceCracker.G;
import com.YRH.Others.GameData;
import com.YRH.Others.GameSound;
import com.YRH.Scenes.PlayScene;

public class IceSprite extends CCSprite {
	public boolean isActive;
	public int 	iceKind;
	int 		_iceType;

	boolean isSplit;
	boolean isSplitFirst;
	boolean isSplitSecond;
	boolean isMultiSplit;
	
	CCSprite oneSprite;
	CCSprite firstPartSprite;
	CCSprite firstPartFirstSprite;
	CCSprite firstPartSecondSprite;
	CCSprite secondPartSprite;
	CCSprite secondPartFirstSprite;
	CCSprite secondPartSecondSprite;
    CCSprite penguin1Oh1Sprite;
    CCSprite penguin1Oh2Sprite;
    CCSprite penguin1Oh3Sprite;
    CCSprite penguin2Oh1Sprite;
    CCSprite penguin2Oh2Sprite;
    CCSprite penguin2Oh3Sprite;
	
    int penguin1SlashCount, penguin2SlashCount;
	float vX, vY, vRotate, pX, pY;
	float rotateOne;
	float rotateFirstPart,rotateSecondPart;
	float firstPartVX, firstPartVY;
	float firstPartFirstVX, firstPartFirstVY;
	float firstPartSecondVX, firstPartSecondVY;
	float secondPartVX, secondPartVY;
	float secondPartFirstVX, secondPartFirstVY;
	float secondPartSecondVX, secondPartSecondVY;
	
	float firstPartPX, firstPartPY;
	float _firstPartFirstPX, _firstPartFirstPY;
	float _firstPartSecondPX, _firstPartSecondPY;
	float secondPartPX, secondPartPY;
	float _secondPartFirstPX, _secondPartFirstPY;
	float _secondPartSecondPX, _secondPartSecondPY;
	
	float flyingTime, splitTime, _delayTime;


	public IceSprite() {
		super("ice1@2x.png"); G.setScale(this);
		setVisible(false);
		isSplit = false;
		isActive = false;
		isMultiSplit = GameData.isMultiSplit();
		
		oneSprite = CCSprite.sprite("ice1@2x.png");
		addChild(oneSprite, 0);
		
		firstPartSprite = CCSprite.sprite("ice1_2_pieces_part1@2x.png");
		addChild(firstPartSprite, 1);

		secondPartSprite = CCSprite.sprite("ice1_2_pieces_part2@2x.png");
		addChild(secondPartSprite, 2);
		
		firstPartFirstSprite = CCSprite.sprite("ice1_4_pieces_part1@2x.png");
		addChild(firstPartFirstSprite, 3);
		
		firstPartSecondSprite = CCSprite.sprite("ice1_4_pieces_part2@2x.png");
		addChild(firstPartSecondSprite, 4);
		
		
		secondPartFirstSprite = CCSprite.sprite("ice1_4_pieces_part3@2x.png");
		addChild(secondPartFirstSprite, 5);
		
		secondPartSecondSprite = CCSprite.sprite("ice1_4_pieces_part4@2x.png");
		addChild(secondPartSecondSprite, 6);

        penguin1Oh1Sprite = CCSprite.sprite("penguin1_Oh1@2x.png");
        addChild(penguin1Oh1Sprite, 7);

        penguin1Oh2Sprite = CCSprite.sprite("penguin1_Oh2@2x.png");
        addChild(penguin1Oh2Sprite, 8);

        penguin1Oh3Sprite = CCSprite.sprite("penguin1_Oh3@2x.png");
        addChild(penguin1Oh3Sprite, 9);

		penguin2Oh1Sprite = CCSprite.sprite("penguin2_Oh1@2x.png");
        addChild(penguin2Oh1Sprite, 10);

        penguin2Oh2Sprite = CCSprite.sprite("penguin2_Oh2@2x.png");
        addChild(penguin2Oh2Sprite, 11);

        penguin2Oh3Sprite = CCSprite.sprite("penguin2_Oh3@2x.png");
        addChild(penguin2Oh3Sprite, 12);

		initIce();
	}
	
	public void initIce() {
        setPosition(0, G._getPosY(Y_START));
		oneSprite.setPosition(0, 0);
		firstPartSprite.setPosition(0, 0);
		firstPartFirstSprite.setPosition(0, 0);
		firstPartSecondSprite.setPosition(0, 0);
		secondPartSprite.setPosition(0, 0);
		secondPartFirstSprite.setPosition(0, 0);
		secondPartSecondSprite.setPosition(0, 0);
	    penguin1Oh1Sprite.setPosition(0, 0);
	    penguin1Oh2Sprite.setPosition(0, 0);
	    penguin1Oh3Sprite.setPosition(0, 0);
	    penguin2Oh1Sprite.setPosition(0, 0);
	    penguin2Oh2Sprite.setPosition(0, 0);
	    penguin2Oh3Sprite.setPosition(0, 0);
		isActive = isSplitSecond = 
		isSplitFirst = isSplit = false;
		oneSprite.setVisible(false);
		firstPartSprite.setVisible(false);
		secondPartSprite.setVisible(false);
		firstPartFirstSprite.setVisible(false);
		firstPartSecondSprite.setVisible(false);
		secondPartFirstSprite.setVisible(false);
		secondPartSecondSprite.setVisible(false);
	    penguin1Oh1Sprite.setVisible(false);
	    penguin1Oh2Sprite.setVisible(false);
	    penguin1Oh3Sprite.setVisible(false);
	    penguin2Oh1Sprite.setVisible(false);
	    penguin2Oh2Sprite.setVisible(false);
	    penguin2Oh3Sprite.setVisible(false);
		vY = 0; vX = 0;
	    penguin1SlashCount = 0;
	    penguin2SlashCount = 0;
	}
	
	public void settingOpacity(int value) {
		setOpacity(value);
		oneSprite.setOpacity(value);
		firstPartSprite.setOpacity(value);
		firstPartFirstSprite.setOpacity(value);
		firstPartSecondSprite.setOpacity(value);
		secondPartSprite.setOpacity(value);
		secondPartFirstSprite.setOpacity(value);
		secondPartSecondSprite.setOpacity(value);
	}
	
	public void setIceKind(int kind) {
		iceKind = kind;
		firstPartVX = 0; 	firstPartVY = 0;
		secondPartVX = 0; 	secondPartVY = 0;
		firstPartPX = 0; 	firstPartPY = 0;
		secondPartPX = 0; 	secondPartPY = 0;
	}
	
	public void setPosition(float posX, float posY) {
		pX = posX; pY = posY;
		super.setPosition(posX, posY);
	}
	
	public void setVelocity(float vx, float vy) {
		vX = vx; vY = vy;
	}
	
	public void scheduler() {
		flyingTime = 0;
		splitTime = flyingTime;
		schedule("update", G.TIME_TICKS);
	}
	
	public void startFlying(float delay) {
		if (isActive) return;
		initIce();
		isActive = true;
		float px2, height, time;
		setIceKind(MathLib.random(0, MAX_ICE-2));
	    
		CCSprite tmp = CCSprite.sprite(String.format("%s@2x.png", Ices_Name[iceKind]));
		oneSprite.setTexture(tmp.getTexture());
		oneSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
		tmp = null; oneSprite.setOpacity(255); oneSprite.setVisible(true);
	
		tmp = CCSprite.sprite(String.format("%s_2_pieces_part1@2x.png", Ices_Name[iceKind]));
	    firstPartSprite.setTexture(tmp.getTexture());
		firstPartSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
		tmp = null; firstPartSprite.setOpacity(255); firstPartSprite.setVisible(false);
		
	    tmp = CCSprite.sprite(String.format("%s_4_pieces_part1@2x.png", Ices_Name[iceKind]));
		firstPartFirstSprite.setTexture(tmp.getTexture());
		firstPartFirstSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
		tmp = null; firstPartFirstSprite.setOpacity(255); firstPartFirstSprite.setVisible(false);
	
		tmp = CCSprite.sprite(String.format("%s_4_pieces_part2@2x.png", Ices_Name[iceKind]));
		firstPartSecondSprite.setTexture(tmp.getTexture());
		firstPartSecondSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
		tmp = null; firstPartSecondSprite.setOpacity(255); firstPartSecondSprite.setVisible(false);
	
		tmp = CCSprite.sprite(String.format("%s_2_pieces_part2@2x.png", Ices_Name[iceKind]));
		secondPartSprite.setTexture(tmp.getTexture());
		secondPartSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
		tmp = null; secondPartSprite.setOpacity(255); secondPartSprite.setVisible(false);
	
		tmp = CCSprite.sprite(String.format("%s_4_pieces_part3@2x.png", Ices_Name[iceKind]));
		secondPartFirstSprite.setTexture(tmp.getTexture());
		secondPartFirstSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
		tmp = null; secondPartFirstSprite.setOpacity(255); secondPartFirstSprite.setVisible(false);
		
	    tmp = CCSprite.sprite(String.format("%s_4_pieces_part4@2x.png", Ices_Name[iceKind]));
		secondPartSecondSprite.setTexture(tmp.getTexture());
		secondPartSecondSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
		tmp = null; secondPartSecondSprite.setOpacity(255); secondPartSecondSprite.setVisible(false);
		
		System.gc();
		
		pX = MathLib.random(G.WIN_W / 10, G.WIN_W * 9 / 10);
        pY = G._getPosY(Y_START);
		px2 = MathLib.random(G.WIN_W / 10, G.WIN_W * 9 / 10);
		height = MathLib.random(
			G.WIN_H * FLYING_HEIGHT_MIN / 10,
			G.WIN_H * FLYING_HEIGHT_MAX / 10);
        vY = (float) MathLib.sqrt((height - pY) * 4 * GRAVITY);
        time = vY / GRAVITY * 2;
		vX = (px2 - pX) / time / 2;
		vRotate = MathLib.random(-10, 10);
		setRotation(0);
		
		float delayT = MathLib.random(0, delay);
		isSplit = false;
		setVisible(true);
		runAction(CCSequence.actions(
			 CCDelayTime.action(delayT),
			 CCCallFunc.action(this, "scheduler")));
	}
	
	public void startFlyingWithPenguin(float delay, int penguinRate) {
		if (isActive)  return;
		initIce(); isActive = true;
		float px2, height, time;
		setIceKind(MathLib.min(MathLib.random(0, MAX_ICE + penguinRate), MAX_ICE));
	
		CCSprite tmp;
		if (iceKind != MAX_ICE && iceKind != MAX_ICE-1) {
	        tmp = CCSprite.sprite(String.format("%s@2x.png", Ices_Name[iceKind]));
			oneSprite.setTexture(tmp.getTexture());
			oneSprite.setTextureRect(tmp.getTextureRect()); tmp = null;

			tmp = CCSprite.sprite(String.format("%s_2_pieces_part1@2x.png", Ices_Name[iceKind]));
			firstPartSprite.setTexture(tmp.getTexture());
			firstPartSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
	
			tmp = CCSprite.sprite(String.format("%s_4_pieces_part1@2x.png", Ices_Name[iceKind]));
			firstPartFirstSprite.setTexture(tmp.getTexture());
			firstPartFirstSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
	
			tmp = CCSprite.sprite(String.format("%s_4_pieces_part2@2x.png", Ices_Name[iceKind]));
			firstPartSecondSprite.setTexture(tmp.getTexture());
			firstPartSecondSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
	
			tmp = CCSprite.sprite(String.format("%s_2_pieces_part2@2x.png", Ices_Name[iceKind]));
			secondPartSprite.setTexture(tmp.getTexture());
			secondPartSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
	
			tmp = CCSprite.sprite(String.format("%s_4_pieces_part3@2x.png", Ices_Name[iceKind]));
			secondPartFirstSprite.setTexture(tmp.getTexture());
			secondPartFirstSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
	
			tmp = CCSprite.sprite(String.format("%s_4_pieces_part4@2x.png", Ices_Name[iceKind]));
			secondPartSecondSprite.setTexture(tmp.getTexture());
			secondPartSecondSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
	
	    } else {
	        tmp = CCSprite.sprite(String.format("%s@2x.png", Ices_Name[iceKind]));
			oneSprite.setTexture(tmp.getTexture());
			oneSprite.setTextureRect(tmp.getTextureRect()); tmp = null;
		}
	
		oneSprite.setVisible(true); 			oneSprite.setOpacity(255);
		firstPartSprite.setVisible(false);		firstPartSprite.setOpacity(255);
		firstPartFirstSprite.setVisible(false);firstPartFirstSprite.setOpacity(255);
		firstPartSecondSprite.setVisible(false);firstPartSecondSprite.setOpacity(255);
		secondPartSprite.setVisible(false);	secondPartSprite.setOpacity(255);
		secondPartSecondSprite.setVisible(false);secondPartFirstSprite.setOpacity(255);
		secondPartSecondSprite.setVisible(false);secondPartSecondSprite.setOpacity(255);
	
		pX = MathLib.random(G.WIN_W / 10, G.WIN_W * 9 / 10);
        pY = G._getPosY(Y_START);
		px2 = MathLib.random(G.WIN_W / 10, G.WIN_W * 9 / 10);
		height = MathLib.random(
			G.WIN_H * FLYING_HEIGHT_MIN / 10,
			G.WIN_H * FLYING_HEIGHT_MAX / 10);
        vY = (float) MathLib.sqrt((height - pY) * 4 * GRAVITY);
        time = 2 * vY / GRAVITY;
		vX = (px2 - pX) / time / 2;
		vRotate = MathLib.random(-10, 10);
		_delayTime = MathLib.random(0, delay);
		if (iceKind == MAX_ICE && iceKind == MAX_ICE-1) _delayTime = 0.6f;
		isSplit = false; setVisible(true);
		runAction(
			CCSequence.actions(
			CCDelayTime.action(_delayTime),
			CCCallFunc.action(this, "scheduler")));
	}
	
	
	public void update(float deltaTime) {
		if (!isActive) return;
	
		if (iceKind != MAX_ICE || iceKind != MAX_ICE-1) {
			if (flyingTime < G.TIME_TICKS) {
	            GameSound.effectThrowIce(iceKind);
	        }
		}
	
		flyingTime += deltaTime;
		float time = deltaTime / G.TIME_TICKS;
        pY += vY * time - time * time * GRAVITY*2 / 2;
        vY -= time * GRAVITY*2;
		pX += vX * time;
	    setPosition(pX, pY);
	
		if (isSplit) {
			firstPartPX += firstPartVX;
			firstPartPY += firstPartVY;
			secondPartPX += secondPartVX;
			secondPartPY += secondPartVY;
			firstPartSprite.setPosition(firstPartPX, firstPartPY);
			secondPartSprite.setPosition(secondPartPX, secondPartPY);
			
			if (isSplitFirst) {
				_firstPartFirstPX += firstPartFirstVX;
				_firstPartFirstPY += firstPartFirstVY;
				_firstPartSecondPX += firstPartSecondVX;
				_firstPartSecondPY += firstPartSecondVY;
				firstPartFirstSprite.setPosition(
					firstPartPX + _firstPartFirstPX,
					firstPartPY + _firstPartFirstPY);
				firstPartSecondSprite.setPosition(
					firstPartPX + _firstPartSecondPX,
					firstPartPY + _firstPartSecondPY);
			}
			
			if (isSplitSecond) {
				_secondPartFirstPX += secondPartFirstVX;
				_secondPartFirstPY += secondPartFirstVY;
				_secondPartSecondPX += secondPartSecondVX;
				_secondPartSecondPY += secondPartSecondVY;
				secondPartFirstSprite.setPosition(
					secondPartPX + _secondPartFirstPX,
					secondPartPY + _secondPartFirstPY);
				secondPartSecondSprite.setPosition(
					secondPartPX + _secondPartSecondPX,
					secondPartPY + _secondPartSecondPY);
			}
			
			if (vY < 0 && pY < G._getPosY(Y_START)) {
				unschedule("update");
				initIce();
			}
		} else {
			if (pY < G._getPosY(Y_START)) {
				unschedule("update");
				initIce();
			}
		}
	}
	
	public int splitAction(float posX1, float posY1, float posX2, float posY2) {
		int retval = enum_split_none;
		float x, y, x1, y1, x2, y2;
		float width, height;
        if ((MathLib.abs(posX2-posX1) < G._getPosX(10) ||
        	MathLib.abs(posY2-posY1) < G._getPosX(10)) ||
        	!isActive || splitTime > flyingTime - 0.2f)
			return enum_split_none;
        
		if (!isSplit) {
            width = getWidth(
            	oneSprite.getContentSize().width,
            	oneSprite.getContentSize().height, -getRotation());
            height = getHeight(
            	oneSprite.getContentSize().width,
            	oneSprite.getContentSize().height, -getRotation());
	            
	        x1 = getPosition().x - width / 2; 	x2 = getPosition().x + width / 2;
	        y1 = getPosition().y - height / 2; 	y2 = getPosition().y + height / 2;
	
			for (int i = 0; i <= MAX_STEP; i++) {
				x = (posX1 * (MAX_STEP - i) + posX2 * i) / MAX_STEP;
				y = (posY1 * (MAX_STEP - i) + posY2 * i) / MAX_STEP;
				if (x1 < x && x < x2 && y1 < y && y < y2) {
					splitTime = flyingTime;
					if (iceKind == MAX_ICE) {
						GameSound.vib(GameSound.VIB_ICE);
	                    penguin2SlashCount++;
	                    switch(penguin2SlashCount) {
	                    case 1:
	                        oneSprite.setVisible(false);
	                        penguin2Oh1Sprite.setVisible(true);
	                        penguin2Oh1Sprite.setPosition(oneSprite.getPosition());
	                        break;
	                    case 2:
	                        penguin2Oh1Sprite.setVisible(false);
	                        penguin2Oh2Sprite.setVisible(true);
	                        penguin2Oh2Sprite.setPosition(penguin2Oh1Sprite.getPosition());
	                        break;
	                    case 3:
	                        penguin2Oh2Sprite.setVisible(false);
	                        penguin2Oh3Sprite.setVisible(true);
	                        penguin2Oh3Sprite.setPosition(penguin2Oh2Sprite.getPosition());
	                        ((PlayScene) getParent()).decreasePenguinLife();
	                        break;
	                    }
	                        
	                    GameSound.effectPenguin();
						return enum_split_none;
					} else if (iceKind == MAX_ICE-1) {
	                    penguin1SlashCount++;
	                    switch(penguin1SlashCount) {
	                    case 1:
	                        oneSprite.setVisible(false);
	                        penguin1Oh1Sprite.setVisible(true);
	                        penguin1Oh1Sprite.setPosition(oneSprite.getPosition());
	                        break;
	                    case 2: 
	                        penguin1Oh1Sprite.setVisible(false);
	                        penguin1Oh2Sprite.setVisible(true);
	                        penguin1Oh2Sprite.setPosition(penguin1Oh1Sprite.getPosition());
	                        break;
	                    case 3:
	                        penguin1Oh2Sprite.setVisible(false);
	                        penguin1Oh3Sprite.setVisible(true);
	                        penguin1Oh3Sprite.setPosition(penguin1Oh2Sprite.getPosition());
	                        ((PlayScene) getParent()).decreasePenguinLife();
	                        break;
	                    }
	
	                    GameSound.effectPenguin();
	                    GameSound.vib(GameSound.VIB_BOMB);
						return enum_split_none;
					}
					
					oneSprite.setVisible(false);
					firstPartSprite.setVisible(true);
					secondPartSprite.setVisible(true);
					vY /= 4; vX /= 4;
					float alpha = (float) MathLib.toDegrees(
						MathLib.atan2(posY2 - posY1, posX2 - posX1));
					
					switch (iceKind) {
					case 0: case 1: case 2: case 6: case 8:
						if (MathLib.abs(((int)alpha % 360) - ((int)(-getRotation() + 90) % 360)) > 90) {
							setRotation(-(alpha - 90 + 180));
						}
						else setRotation(-(alpha - 90));
						firstPartPY = secondPartPY = 0;
                        firstPartPX = -firstPartSprite.getContentSize().width / 2;
                        secondPartPX = secondPartSprite.getContentSize().width / 2;
						firstPartVX = -MathLib.random(1, 3);
						secondPartVX = -firstPartVX;
						firstPartVY = secondPartVY = 0;
						break;
                    case 10: break;
					default:
						if (MathLib.abs((((int)alpha) % 360) - ((int)(-getRotation()) % 360)) > 90)
							setRotation(-(alpha + 180));
						else 
                            setRotation(-alpha);

                        firstPartPY = firstPartSprite.getContentSize().height / 2;
                        secondPartPY = -secondPartSprite.getContentSize().height / 2;
						firstPartPX = secondPartPX = firstPartVX = secondPartVX = 0;
						firstPartVY = MathLib.random(1, 3); secondPartVY = -firstPartVY;
						break;
					}
					firstPartSprite.setPosition(
						firstPartPX, firstPartPY);
					secondPartSprite.setPosition(
						secondPartPX, secondPartPY);
					
					isSplit = true;
					
					GameSound.effectIceSlashed(iceKind);
	                GameSound.vib(1);
					return enum_first_split;
				}
			}
		}
		else {
			if (!isMultiSplit) {
	            return retval;
	        }
	        if (MathLib.abs(posX2-posX1) <= 5 || MathLib.abs(posY2-posY1) <= 5) {
	            return retval;
	        }
			float alpha = - (float) MathLib.toRadians(getRotation());
			if (!isSplitFirst) {
                width = getWidth(
                	firstPartSprite.getContentSize().width,
                	firstPartSprite.getContentSize().height, -getRotation());
                height = getHeight(
                	firstPartSprite.getContentSize().width,
                	firstPartSprite.getContentSize().height, -getRotation());
				x1 = getPosition().x +
					(float)(firstPartPX * MathLib.cos(alpha) +
					firstPartPY * MathLib.sin(alpha) - width / 2) * getScaleX();
				x2 = x1 + width * getScaleX();
				y1 = getPosition().y +
					(float)(-firstPartPX * MathLib.sin(alpha) +
					firstPartPY * MathLib.cos(alpha) - height / 2) * getScaleY();
				y2 = y1 + height * getScaleY();
				
				for (int i = 0; i <= MAX_STEP; i++) {
					x = (posX1 * (MAX_STEP - i) + posX2 * i) / MAX_STEP;
					y = (posY1 * (MAX_STEP - i) + posY2 * i) / MAX_STEP;
					if (x1 < x && x < x2 && y1 < y && y < y2) {
						splitTime = flyingTime;
						firstPartSprite.setVisible(false);
						firstPartFirstSprite.setVisible(true);
						firstPartSecondSprite.setVisible(true);
						vY /= 4; vX /= 4;
						alpha = (float) MathLib.toDegrees(MathLib.atan2(posY2 - posY1, posX2 - posX1));
						if (MathLib.abs((alpha % 360) - ((-getRotation() + 90) % 360)) > 90) {
							firstPartFirstSprite.setRotation(-(alpha - 90 + 180) - getRotation());
						}
						else {
	                        firstPartFirstSprite.setRotation(-(alpha - 90) - getRotation());
	                    }
						alpha = -(float) MathLib.toRadians(firstPartFirstSprite.getRotation());
						firstPartSecondSprite.setRotation(firstPartFirstSprite.getRotation());
						
                        _firstPartFirstPX = -firstPartFirstSprite.getContentSize().width / 2 * (float)MathLib.cos(alpha);
                        _firstPartFirstPY = -firstPartFirstSprite.getContentSize().width / 2 * (float)MathLib.sin(alpha);
                        _firstPartSecondPX = firstPartSecondSprite.getContentSize().width / 2 * (float)MathLib.cos(alpha);
                        _firstPartSecondPY = firstPartSecondSprite.getContentSize().width / 2 * (float)MathLib.sin(alpha);
						firstPartFirstVX = -MathLib.random(1, 3) * (float) MathLib.cos(alpha);
						firstPartFirstVY = -MathLib.random(1, 3) * (float) MathLib.sin(alpha);
						firstPartSecondVX = MathLib.random(1, 3) * (float) MathLib.cos(alpha);
						firstPartSecondVY = MathLib.random(1, 3) * (float) MathLib.sin(alpha);
						firstPartFirstSprite.setPosition(
							_firstPartFirstPX + firstPartPX,
							_firstPartFirstPY + firstPartPY);
						firstPartSecondSprite.setPosition(
							_firstPartSecondPX + firstPartPX,
							_firstPartSecondPY + firstPartPY);
						isSplitFirst = true;
						retval = enum_second_first_split;
						break;
					}
				}
			}
			alpha =  -(float)MathLib.toRadians(getRotation());
			if (!isSplitSecond) {
	                width = getWidth(
	                	secondPartSprite.getContentSize().width,
	                	secondPartSprite.getContentSize().height, -getRotation());
	                height = getHeight(
	                	secondPartSprite.getContentSize().width,
	                	secondPartSprite.getContentSize().height, -getRotation());
					x1 = getPosition().x + 
						(float)(secondPartPX * MathLib.cos(alpha) + 
						secondPartPY * MathLib.sin(alpha) - width / 2) * getScaleX();
					y1 = getPosition().y + 
						(float)(-secondPartPX * MathLib.sin(alpha) +
						secondPartPY * MathLib.cos(alpha) - height / 2) * getScaleY();
					x2 = x1 + width * getScaleX(); y2 = y1 + height * getScaleY();
				
				for (int i = 0; i <= MAX_STEP; i++) {
					x = (posX1 * (MAX_STEP - i) + posX2 * i) / MAX_STEP;
					y = (posY1 * (MAX_STEP - i) + posY2 * i) / MAX_STEP;
					if (x1 < x && x < x2 && y1 < y && y < y2) {
						splitTime = flyingTime;
						secondPartSprite.setVisible(false);
						secondPartFirstSprite.setVisible(true);
						secondPartSecondSprite.setVisible(true);
						vY /= 4; vX /= 4;
						alpha = (float) MathLib.toDegrees(
							MathLib.atan2(posY2 - posY1, posX2 - posX1));
						if (MathLib.abs(((int)alpha % 360) - ((int)(-getRotation() + 90) % 360)) > 90)
							secondPartFirstSprite.setRotation(
								-(alpha + 90) - getRotation());
						else 
	                        secondPartFirstSprite.setRotation(-(alpha - 90) - getRotation());
						alpha = -(float)MathLib.toRadians(secondPartFirstSprite.getRotation());
						secondPartSecondSprite.setRotation(secondPartFirstSprite.getRotation());
						
                        _secondPartFirstPX = secondPartFirstSprite.getContentSize().width / 2 * (float)MathLib.cos(alpha);
                        _secondPartFirstPY = secondPartFirstSprite.getContentSize().width / 2 * (float)MathLib.sin(alpha);
                        _secondPartSecondPX = secondPartSecondSprite.getContentSize().width / 2 * (float)MathLib.cos(alpha);
                        _secondPartSecondPY = secondPartSecondSprite.getContentSize().width / 2 * (float)MathLib.sin(alpha);
						secondPartFirstVX = -MathLib.random(1, 3) * (float)MathLib.cos(alpha);
						secondPartFirstVY = -MathLib.random(1, 3) * (float)MathLib.sin(alpha);
						secondPartSecondVX = MathLib.random(1, 3) * (float)MathLib.cos(alpha);
						secondPartSecondVY = MathLib.random(1, 3) * (float)MathLib.sin(alpha);
						secondPartFirstSprite.setPosition(
							_secondPartFirstPX + secondPartPX,
							_secondPartFirstPY + secondPartPY);
						secondPartSecondSprite.setPosition(
							_secondPartSecondPX + secondPartPX,
							_secondPartSecondPY + secondPartPY);
						isSplitSecond = true;
						retval += enum_second_second_split;
						break;
					}
				}
			}
			if (retval != enum_split_none) {
	            GameSound.effectIceSlashed(iceKind);
	        }
		}
		return retval;
	}
	
	public float getWidth(float width, float height, float alpha) {
		alpha = (float) MathLib.toRadians(alpha);
		float x1 = width * (float) MathLib.cos(alpha) + height * (float) MathLib.sin(alpha);
		float x2 = -width * (float) MathLib.cos(alpha) + height * (float) MathLib.sin(alpha);
		float x3 = width * (float) MathLib.cos(alpha) - height * (float) MathLib.sin(alpha);
		float x4 = -width * (float) MathLib.cos(alpha) - height * (float) MathLib.sin(alpha);
		float dd = MathLib.max(MathLib.max(x1, x2), MathLib.max(x3, x4));
		return dd;
	}
	
	public float getHeight(float width, float height, float alpha) {
		alpha = (float) MathLib.toRadians(alpha);
		float x1 = width * (float) MathLib.sin(alpha) + height * (float) MathLib.cos(alpha);
		float x2 = -width * (float) MathLib.sin(alpha) + height * (float) MathLib.cos(alpha);
		float x3 = width * (float) MathLib.sin(alpha) - height * (float) MathLib.cos(alpha);
		float x4 = -width * (float) MathLib.sin(alpha) - height * (float) MathLib.cos(alpha);
		float dd = MathLib.max(MathLib.max(x1, x2), MathLib.max(x3, x4));
		return dd;
	}
	
	@Override public void onExit() {
		oneSprite.removeFromParentAndCleanup(true);
		firstPartSprite.removeFromParentAndCleanup(true);
		firstPartFirstSprite.removeFromParentAndCleanup(true);
		firstPartSecondSprite.removeFromParentAndCleanup(true);
		secondPartSprite.removeFromParentAndCleanup(true);
		secondPartFirstSprite.removeFromParentAndCleanup(true);
		secondPartSecondSprite.removeFromParentAndCleanup(true);
	    penguin1Oh1Sprite.removeFromParentAndCleanup(true);
	    penguin1Oh2Sprite.removeFromParentAndCleanup(true);
	    penguin1Oh3Sprite.removeFromParentAndCleanup(true);
	    penguin2Oh1Sprite.removeFromParentAndCleanup(true);
	    penguin2Oh2Sprite.removeFromParentAndCleanup(true);
	    penguin2Oh3Sprite.removeFromParentAndCleanup(true);
	    
	    oneSprite = null;
	    firstPartSprite = null; firstPartFirstSprite = null; firstPartSecondSprite = null;
	    secondPartSprite = null; secondPartFirstSprite = null; secondPartSecondSprite = null;
	    penguin1Oh1Sprite = null; penguin1Oh2Sprite = null; penguin1Oh3Sprite = null;
	    penguin2Oh1Sprite = null; penguin2Oh2Sprite = null; penguin2Oh3Sprite = null;
		super.onExit();
		System.gc();
	}

	public boolean isFullSplit() {
		return isSplitFirst && isSplit && isSplitSecond;
	}
	
	String[] Ices_Name = {
		"ice1", "ice2", "ice3", "ice4", "ice5",
		"ice6", "ice7", "ice8", "ice9", "ice10",
		"penguin1", "penguin2",
	};

	private static final int FLYING_HEIGHT_MIN			= 6;
	private static final int FLYING_HEIGHT_MAX			= 9;
	private static final float GRAVITY					= 0.4f;
	private static final int MAX_STEP					= 25;
	
    public static final int enum_split_none 			= 0;
	public static final int enum_first_split 			= 1;
	public static final int enum_second_first_split 	= 2;
	public static final int enum_second_second_split 	= 4;
	
	public static final int Y_START						= -160;	
	public static final int MAX_ICE						= 11;
}