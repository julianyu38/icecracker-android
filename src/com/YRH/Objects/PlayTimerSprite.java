package com.YRH.Objects;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCScaleTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.types.CGRect;

import com.YRH.IceCracker.G;
import com.YRH.Others.GameSound;

public class PlayTimerSprite extends CCSprite {
	CCSprite[] mDigit = new CCSprite[4];
    
	float time;
	float m_fWidth;
	float m_fHeight;

	public PlayTimerSprite() {
		super("play_count_number@2x.png");
		G.setScale(this); time = 0;
		CCSprite numberSet = CCSprite.sprite("play_count_number@2x.png");
		m_fWidth = numberSet.getContentSize().width / 11;
		m_fHeight = numberSet.getContentSize().height;
		setTextureRect(10 * m_fWidth, 0, m_fWidth, m_fHeight, false);
		
		mDigit[0] = CCSprite.sprite(numberSet.getTexture(),
			CGRect.make(0, 0, m_fWidth, m_fHeight));
        mDigit[0].setPosition(
        	m_fWidth / 2 + G.PadVSPhoneX * DIGIT_X,
        	m_fHeight / 2 + G.PadVSPhoneY * DIGIT_Y);
		addChild(mDigit[0]);

		mDigit[1] = CCSprite.sprite(numberSet.getTexture(),
			CGRect.make(0, 0, m_fWidth, m_fHeight));
        mDigit[1].setPosition(
        	m_fWidth / 2 + G.PadVSPhoneX * (DIGIT_X + DIGIT_WIDTH),
        	m_fHeight / 2 + G.PadVSPhoneY * DIGIT_Y);
		addChild(mDigit[1]);

		mDigit[2] = CCSprite.sprite(numberSet.getTexture(),
			CGRect.make(0, 0, m_fWidth, m_fHeight));
        mDigit[2].setPosition(
        	m_fWidth / 2 + G.PadVSPhoneX * (DIGIT_X + 3 * DIGIT_WIDTH),
        	m_fHeight / 2 + G.PadVSPhoneY * DIGIT_Y);
		addChild(mDigit[2]);

		mDigit[3] = CCSprite.sprite(numberSet.getTexture(),
			CGRect.make(0, 0, m_fWidth, m_fHeight));
        mDigit[3].setPosition(
        	m_fWidth / 2 + G.PadVSPhoneX * (DIGIT_X + 4 * DIGIT_WIDTH),
        	m_fHeight / 2 + G.PadVSPhoneY * DIGIT_Y);
		addChild(mDigit[3]);
		refresh();
	}
	
	public void settingOpacity(int value) {
		setOpacity(value);
		for (int i = 0; i < mDigit.length; i++) 
			mDigit[i].setOpacity(value);
	}
	public void refresh() {
		if (time >= 200) return;
		int num;
		if (time >= 600)mDigit[0].setVisible(true);
		else			mDigit[0].setVisible(false);
	
		num = ((int)(time / 600.0)) % 10;
		mDigit[0].setTextureRect(num * m_fWidth, 0, m_fWidth, m_fHeight, false);
		num = ((int)(time / 60)) % 10;
		mDigit[1].setTextureRect(num * m_fWidth, 0, m_fWidth, m_fHeight, false);
		num = ((int)(time / 10)) % 6;
		mDigit[2].setTextureRect(num * m_fWidth, 0, m_fWidth, m_fHeight, false);
		num = ((int)time) % 10;
		mDigit[3].setTextureRect(num * m_fWidth, 0, m_fWidth, m_fHeight, false);
	}
	
	public void initTimer() {
		time = 0;
		CCSprite tmp = CCSprite.sprite("play_count_number@2x.png");
		for(int i=0; i<mDigit.length; i++) {
			 mDigit[i].setTexture(tmp.getTexture());
			 mDigit[i].setTextureRect(tmp.getTextureRect());
		} tmp = null;
		refresh();
	}
	
	public void clearTimer() {
		time = 0; refresh();
	}
	
	public void bitLarger() {
		runAction(CCScaleTo.action(0.2f*G._scaleX, 1.25f*G._scaleX));
	}
	
	public void bitSmaller() {
		runAction(CCScaleTo.action(0.2f*G._scaleX, G._scaleX));
	}
	
	public void setTime(float value) {
		if ((int)time != (int)value) {
			time = (int)value;
			if (time == 20) {
				CCSprite tmp = CCSprite.sprite("play_count_number_red@2x.png");
				for(int i=0; i<mDigit.length; i++) {
					 mDigit[i].setTexture(tmp.getTexture());
					 mDigit[i].setTextureRect(tmp.getTextureRect());
				} tmp = null;
			}
			refresh();
			if (time <= 30) {
				if (time <= 5) {
	                GameSound.effectBeep();
	            }
				runAction(CCSequence.actions(
					 CCCallFunc.action(this, "bitLarger"),
					 CCDelayTime.action(0.2f),
					 CCCallFunc.action(this, "bitSmaller")));
			}
		}
	}

	@Override public void onExit() {
	    for (int i = 0; i <mDigit.length; i++) {
	        mDigit[i].removeFromParentAndCleanup(true);
	        mDigit[i] = null;
	    }
	    mDigit = null;
		super.onExit();
		System.gc();
	}

	private static final int DIGIT_WIDTH    = 14;
	private static final int DIGIT_X        = -28;
	private static final int DIGIT_Y        = 0;
}